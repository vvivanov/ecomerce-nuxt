export default function (req, res, next) {
  try {
    decodeURIComponent(req.originalUrl);
    // req.url = encodeURI(req.url);

    next();
  } catch (e) {
    const url = encodeURI(unescape(req.originalUrl));

    res.writeHead(302,
      { Location: url }
    );
    res.end();
  }
}
