import { curry, is, apply, omit } from 'ramda'

function decode (string) {
  let result
  try {
    result = decodeURIComponent(string)
  } catch (e) {
    result = unescape(string)
  }
  return result
}

// Perl respects '1' and '' and denies 'true' and 'false'
function cvtBooleanToPerl (v) {
  if (typeof v === 'boolean') {
    return v ? 'true' : ''
  } else if (v === 'false') {
    return ''
  }
  return v
}

// modified parseQuery from vue-router to handle cp1251 chars
export function parseQuery (query) {
  const res = {}
  if (typeof (query) !== 'string') {
    return res
  }
  query = query.trim().replace(/^(\?|#|&)/, '')

  if (!query) {
    return res
  }

  query.split('&').forEach(param => {
    const parts = param.replace(/\+/g, ' ').split('=')
    const key = decode(parts.shift())
    const val = parts.length > 0 ? decode(parts.join('=')) : null

    if (res[key] === undefined) {
      res[key] = val
    } else if (Array.isArray(res[key])) {
      res[key].push(val)
    } else {
      res[key] = [res[key], val]
    }
  })
  return res
}

// TODO: Неиспользуемый метод
// { a: 1, b: 2 } => a=1&b=2
export function buildQueryFromObj (obj) {
  const parts = []
  for (const k in obj) {
    parts.push(encodeURIComponent(k) + '=' + encodeURIComponent(cvtBooleanToPerl(obj[k])))
  }
  return parts.length ? parts.join('&') : ''
}

// 'руб' + plural(5, 'ль' , 'ля', 'лей') = рублей
export function plural (n, one, two, five) {
  if (isNaN(n)) return five
  n = Math.abs(n) % 100
  if ((n > 4 && n < 21) || (n %= 10) > 4 || n === 0) return five
  if (n > 1) return two
  return one
}

/**
 * Debounce
 *
 * @param {Boolean} immediate If true run `fn` at the start of the timeout
 * @param  timeMs {Number} Debounce timeout
 * @param  fn {Function} Function to debounce
 *
 * @return {Number} timeout
 * @example
 *
 *      const say = (x) => console.log(x)
 *      const debouncedSay = debounce_(false, 1000, say)();
 *
 *      debouncedSay("1")
 *      debouncedSay("2")
 *      debouncedSay("3")

 *      const deboucedAjax = debounce(300, $.ajax)()
 *      deboucedAjax({ url: ... })
 *
 */
export const debounce_ = curry((immediate, timeMs, fn) => () => {
  let timeout

  return (...args) => {
    return new Promise((resolve, reject) => {
      const later = () => {
        timeout = null

        if (!immediate) {
          resolve(apply(fn, args))
        }
      }

      const callNow = immediate && !timeout

      clearTimeout(timeout)
      timeout = setTimeout(later, timeMs)

      if (callNow) {
        resolve(apply(fn, args))
      }
    })
  }
})

export const debounce = debounce_(false)

export const getWindowWidth = () => {
  let width = 0
  if (process.client) {
    width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
  }
  return width
}
// export const getWindowHeight = () => { return Math.max(document.documentElement.clientHeight, window.innerHeight || 0) } // TODO: Неиспользуемый метод

// обертка для lggr
export const logger = (e0, ...args) => {
  if (process.client) {
    if (!window.lggr) {
      // console.error('No lggr loaded!', e0, ...args)
      return
    }
    const lggr = window.lggr
    const eventType = 'vue-' + e0
    if (!args[0] || is(String, args[0])) {
      lggr(eventType, ...args)
    } else { // fallback
      lggr.process_event(eventType, ...args) // 'vue-submit'|'vue-click'|..., event, 'send', ?pos, ?product_id
    }
  }
}

// из структуры с информацией о фильтрах выбираем структуру с выбором пользователя
export function extractUserFilterData (data) {
  const res = {}
  for (const d of data) {
    switch (d.type) {
      case 'range':
        for (const v of d.values) {
          if (v.selected) {
            res[d.name] = res[d.name] || {}
            res[d.name][v.name] = v.value
          }
        }
        break
      case 'select':
      case 'radio':
      case 'checkbox':
        for (const v of d.values) {
          if (v.selected) {
            res[d.name] = res[d.name] || {}
            res[d.name][d.name] = d.type === 'checkbox' ? true : v.value
          }
        }
        break
      case 'checkboxes':
        for (const v of d.values) {
          if (v.selected) {
            res[d.name] = res[d.name] || {}
            res[d.name][v.id] = true
          }
        }
        break
    }
  }
  return res
}

// структуру с "выбор пользователя из фильтров" преобразуем в query string
// { param: val, p253: { p253_min: 10, p253_max: 100 }, p111: { p111: true }, p222: { '12345': true } }
// ==>
//  p253v1=10&p253v2=100&p111=on&p222vTest=on&param=val
export function buildQueryObjFromFilterData (obj) {
  const query = {}
  const query1 = {}
  for (const k in obj) {
    if (is(Object, obj[k])) {
      for (const k1 in obj[k]) {
        const key = k + (k === k1 ? '' : 'v' + (/_min$/.test(k1) ? '1' : /_max$/.test(k1) ? '2' : k1))
        const val = k === k1 || /_(?:min|max)$/.test(k1) ? obj[k][k1] : obj[k][k1] ? 'on' : ''
        query[key] = cvtBooleanToPerl(val)
      }
    } else {
      query1[k] = obj[k]
    }
  }
  return Object.assign(query1, query)
}

// [ { a: 'ymk', b: 'xx', c: 'zz' }, { a: 'ymk', b: 'mm', c: 'nn' } ] => [{ a: 'ymk', values: [ {  b: 'xx', c: 'zz' }, { b: 'mm' c: 'nn' }]
export function groupByFld (fld, lst) {
  const h = lst.reduce((a, v) => {
    const k = v[fld]
    if (k in a.keys) {
      const el = a.res[a.keys[k]]
      a.res[a.keys[k]] = { [fld]: k, values: (el.values || [omit([fld], el)]).concat([omit([fld], v)]) }
    } else {
      a.keys[k] = a.res.length
      a.res.push(v)
    }
    return a
  }, { keys: {}, res: [] })
  return h.res
}

/**
 * Добавляем в массив первую букву для группы
 * @param {Object[]} data список элементов фильтра
 * @param {string} data[].title название фильтра
 * @returns {Object[]} в начале каждой группы добавлена первая буква группы
 */
export function addFirstLetter (data) {
  data.sort((a, b) => a.title > b.title ? 1 : -1) // сортировка по title

  return data.reduce((result, item) => {
    const type = item.title[0] // первая буква текущего названия
    const prevType = result.length > 0 ? result[result.length - 1].title[0] : '' // получаем первую букву предыдущего названия

    prevType !== type && result.push({ groupName: type, top: 0 }) // при смене группы вставляем первую букву

    result.push(item)

    return result
  }, [])
}

//
export const MONTHS = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
const WDAYS_LONG = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']

// TODO: Неиспользуемый метод
// 2020-05-01 -> ['Май 01', 'Пятница']
export function formatDate2 (dateStr) {
  const date = new Date(dateStr)
  let dd = date.getDate()
  if (dd < 10) dd = '0' + dd
  const mm = date.getMonth()
  const wd = date.getDay()
  return [dd + ' ' + MONTHS[mm], WDAYS_LONG[wd]]
}

// for horizontal case!
export function isElementInViewH (element, fullyInView = false, parent = window) {
  const elementOffset = (el) => {
    const rect = el.getBoundingClientRect()
    return {
      top: rect.top + document.body.scrollTop,
      left: rect.left + document.body.scrollLeft
    }
  }

  if (!(element && elementOffset(element))) {
    return false
  }
  // var pageLeft = parent.getBoundingClientRect().left + parent.scrollLeft
  const pageLeft = parent.getBoundingClientRect().left
  // var pageRight = pageLeft + parent.scrollWidth
  const pageRight = parent.getBoundingClientRect().right
  const elementLeft = elementOffset(element).left
  const elementRight = elementLeft + parseFloat(getComputedStyle(element, null).width.replace('px', ''))
  // console.log(parseInt(pageLeft), parseInt(pageRight), 'e', parseInt(elementLeft), parseInt(elementRight))
  if (fullyInView === true) {
    return ((pageLeft <= elementLeft) && (pageRight >= elementRight))
  } else {
    return ((elementLeft <= pageRight) && (elementRight >= pageLeft))
  }
}

// Функция раскодирования спецсимволов HTML
export function decodeHtmlEntities (str) {
  return String(str)
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&apos;/g, '\'')
    .replace(/&bsol;/g, '\\')
    .replace(/&sol;/g, '/')
}

// скопировать str в clipboard
export const copyToClipboard = str => {
  const el = document.createElement('textarea')
  el.value = str
  el.setAttribute('readonly', '')
  el.style.position = 'absolute'
  el.style.left = '-9999px'
  document.body.appendChild(el)
  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)
}

// // RULES: [ ['fld_name', isNumeric] or [function_return_fld_name, isNumeric] ]
// let rules = [[v => { return '19_2' in v && v['19_2'] !== null ? 0 : 1 }, true], ['19_2', false], ['99_56', false]]
// // Usage:
// mylist.sort(rules)
export function sortByRules ([[flf, numeric], ...props]) {
  return function (ah, bh) {
    const a = typeof flf === 'string' ? ah[flf] : flf(ah)
    const b = typeof flf === 'string' ? bh[flf] : flf(bh)
    let equality = 0
    if (numeric) {
      const [aa, bb] = [parseFloat(a), parseFloat(b)].map((v) => isNaN(v) ? 0 : v)
      equality = aa - bb
    } else {
      equality = (a || 'zz').localeCompare(b || 'zz')
    }
    if (equality === 0 && props.length > 0) {
      return sortByRules(props)(ah, bh)
    }
    return equality
  }
}

export const tinySliderCSS = () => {
  const head = document.getElementsByTagName('head')[0]
  const link = document.createElement('link')
  link.rel = 'stylesheet'
  link.type = 'text/css'
  link.href = '/_all/tinyslider/tiny-slider.css'
  head.appendChild(link)
}

/**
 * Добавляем 0 перед числа, если оно до 10
 * @param {number} n - число
 * @returns {string} число
 */
export function addZeros (n) {
  n = String(n)

  while (n.length < 2) {
    n = '0' + n
  }

  return n
}

export function decodeURIComponentSafe (s) {
  if (!s) return s
  return decodeURIComponent(s.replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25'))
}

export const parseServerDate = (d) => {
  const [fdate, ftime] = d.split(' ')
  const fdateList = fdate.split('.')
  const ftimeList = ftime.split(':')
  return new Date(parseInt(fdateList[2]), parseInt(fdateList[1]) - 1, parseInt(fdateList[0]), parseInt(ftimeList[0]), parseInt(ftimeList[1]), 0)
}

const ONE_DAY_MS = 86400000
const THREE_DAYS_MS = ONE_DAY_MS * 3

export const getPromoExtraDates = (item, hasExpiration) => {
  const startDateObj = parseServerDate(item.start)
  const finishDateObj = parseServerDate(item.finish)
  let now = new Date().toLocaleString('en-US', { timeZone: 'Europe/Moscow' })
  now = new Date(now)
  // новые
  const startDiff = Math.round(now.getTime() - startDateObj.getTime())
  const justStarted = startDiff > 0 && startDiff <= THREE_DAYS_MS
  // завершающиеся
  const finishDiff = Math.round(finishDateObj.getTime() - now.getTime())
  const expireSoon = finishDiff > 0 && finishDiff <= THREE_DAYS_MS
  const isCount = finishDiff > 0 && finishDiff <= ONE_DAY_MS
  hasExpiration = hasExpiration || isCount
  const diffTime = new Date(finishDiff)
  const startTimer = [diffTime.getUTCHours(), diffTime.getUTCMinutes(), diffTime.getUTCSeconds()].map(x => x < 10 ? '0' + x : x).join(':')
  return {
    justStarted,
    expireSoon,
    hasExpiration,
    expireTime: isCount ? startTimer : ''
  }
}

/**
 * Склонение числительных
 * @param {number} n
 * @param {array} titles
 * @returns {string}
 */
export const declOfNum = (n, titles) => {
  return titles[(n % 10 === 1 && n % 100 !== 11) ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2]
}

/**
 * @description декоратор который возвращает обёртку, передавая вызов функции
 *  не более одного раза в ms миллисекунд. Т.е вызовы, которые попадают в период «торможения», игнорируются.
 * @param {Function} func исполняемая функция которая передается в обертку
 * @param {*} ms колличество милисекунд через которое будет выполняться функция
 * @returns возвращает обертку через указанное кол-во милисекунд
 */
export function throttle (func, ms) {
  let isThrottled = false
  let savedArgs
  let savedThis

  function wrapper () {
    if (isThrottled) {
      // запоминаем последние аргументы для вызова после задержки
      savedArgs = arguments
      savedThis = this
      return
    }

    // в противном случае переходим в состояние задержки
    func.apply(this, arguments)

    isThrottled = true

    // настройка сброса isThrottled после задержки
    setTimeout(function () {
      isThrottled = false
      if (savedArgs) {
        // если были вызовы, savedThis/savedArgs хранят последний из них
        // рекурсивный вызов запускает функцию и снова устанавливает время задержки
        wrapper.apply(savedThis, savedArgs)
        savedArgs = savedThis = null
      }
    }, ms)
  }

  return wrapper
}

const isDefined = (v) => {
  return v !== null && typeof v !== 'undefined' && v !== ''
}

export const hasFixedSumDelivery = (item) => {
  let ok = isDefined(item.sum_delivery)
  if (!ok) return false
  if ('sum_delivery_fix' in item) {
    ok = '' + item.sum_delivery_fix === '1'
  } else {
    ok = !item['99_11']
  }
  return ok
}

export const hasMinSumDelivery = (item) => {
  let ok = isDefined(item.sum_delivery)
  if (!ok) return false
  if ('sum_delivery_fix' in item) {
    ok = '' + item.sum_delivery_fix === '0'
  } else {
    ok = item['99_11']
  }
  return ok
}

/**
 * Фильтруем список ПВЗ
 * @param {Object[]} pvz список доступных ПВЗ
 * @param {Array{}} filtersData список фильтров для фильтрации
 * @returns {Object[]} список отфильтрованных ПВЗ
 */
export const applyPVZFilters = (pvz, filtersData) => {
  if (filtersData.contractors.length > 0) pvz = pvz.filter(e => e.contractor_name && filtersData.contractors.map(c => c.name).includes(e.contractor_name))
  if (filtersData.dlvTypes.length > 0) pvz = pvz.filter(e => ('99_46' in e) && filtersData.dlvTypes.includes(e['99_46']))
  if (filtersData.dlvDates.length > 0) pvz = pvz.filter(e => e.date_2a && filtersData.dlvDates.includes(e.date_2a))

  if (filtersData.others.length > 0) {
    [pvz] = [...filtersData.others.reduce((sum, current) => sum.add(pvz.filter(item => item[current])), new Set())]
  }

  return pvz
}

export const generatePVZFiltersData = (fs, allContractors) => {
  if (!fs) {
    return {
      contractors: [],
      dlvDates: [],
      dlvTypes: [],
      others: []
    }
  }
  const contractors = fs.contractor_id ? (fs.contractor_id.map(id => allContractors[id]) || []) : []
  const tTexts = { 0: 'Пункт выдачи', 1: 'Постамат' }
  const dlvTypes = (fs['99_46'] || []).map(e => { return { text: tTexts[e], value: e } })
  const dlvDates = fs.date_2a || []

  const others = [
    { value: 'payment_card', text: 'Оплата картой при получении' },
    { value: 'payment_cash', text: 'Оплата наличными при получении' },
    { value: '99_27', text: 'Частичный выкуп' }
  ].filter(e => e.value in fs)

  return {
    contractors,
    dlvDates,
    dlvTypes,
    others
  }
}

// TODO: Неиспользуемый метод
export const arrayToChunck = (arr, chunkSize) => {
  const R = []
  for (let i = 0; i < arr.length; i += chunkSize) { R.push(arr.slice(i, i + chunkSize)) }
  return R
}

/**
 * Формируем список КС
 * @param {Object} locality сырые данные с бека
 * @param {Boolean} showUnavailDlv показать/скрыть недоступные способы доставки
 * @returns {Object[]} список КС
 */
export const getKs = (locality, showUnavailDlv) => {
  if (!locality.d9) return []

  return locality.d9
    .filter(e => showUnavailDlv ? true : e.av !== 0)
    .map(e => Object.assign({ delivery: e.id }, locality[e.id]))
}

export const validForm = (forms, validations, sort) => {
  const valid = []
  Object.keys(forms).forEach(item => {
    const isValid = sort && sort.length > 0 ? sort.includes(item) : Object.keys(validations).includes(item)
    if (isValid) {
      if (forms[item] === null || forms[item] === '') {
        validations[item] = false
        valid.push(item)
      } else {
        validations[item] = true
      }
    }
  })
  return valid.length === 0
}

export const validEmail = value => {
  // eslint-disable-next-line
  const pattern = /^[\w.-]+\@[\w.-]+\.[a-z]{2,7}$/
  return pattern.test(value) || 'Введен некорректный e-mail адрес'
}
