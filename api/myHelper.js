const myHelper = httpClient => ({

  /**
   * Получить список данных по товарам для аналитики.
   * @param { String } listId
   * @returns { Promise }
   */
  getGTMItems (listId) {
    return httpClient.get(`/cgi-bin/lggr_ext.pl?q=lex_items&ids=${listId}`)
  },

  /**
   * Сохранение НП, выбранного пользователем
   * @param { Number } guid
   * @returns { Promise }
   */
  setDeliveryPoints (guid) {
    return httpClient.get(`/cgi-bin/ajax/address.pl?q=store&ao_guid=${guid}`)
  },

  /**
   * Сохранение НП, выбранного пользователем
   * @param { Number } guid
   * @returns { Promise }
   */
  setLocality (guid) {
    return httpClient.get(`/cgi-bin/my_helper.pl?q=change_locality&ao_guid=${guid}`)
  },

  /**
   * Подсказки для выбора населённых пунктов
   * @param { Object } data
   * @returns { Promise }
   */
  searchDeliveryPoints ({ unicode, regionId, term }) {
    return httpClient.get(`/cgi-bin/ajax/address.pl?q=locality&u=${unicode}&region_id_context=${regionId}&term=${term}`)
  },

  /**
   * Поисковые подсказки
   * @param { String } endPoint
   * @returns { Promise }
   */
  searchAutocomplete (endPoint) {
    return httpClient.get(endPoint)
  },

  /**
   * Отправить Форму Обратной Связи
   * @param { String } query
   * @param { String | Number } id
   * @param { Object } data
   * @returns { Promise }
   */
  myHelperPost (query, data, id = '') {
    query = id ? query + '&id=' + id : query
    return httpClient.post(`/cgi-bin/my_helper_post.pl?q=${query}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },

  /**
   * Отправить Отзыв
   * @param { String | Number } id
   * @param { Object } data
   * @returns { Promise }
   */
  addReview (id, data) {
    return httpClient.post(`/cgi-bin/shop_post.pl?q=add_message&id=${id}`, data)
  },

  /**
   * Получить инфо НП
   * @param { String } query
   * @returns { Promise }
   */
  getDeliveryInfo (query) {
    return httpClient.get(`/cgi-bin/my_helper.pl?q=rp&rp_id=${query}`)
  },

  /**
   * Получить меню каталога
   * @param { Object } data
   * @returns { Promise }
   */
  getMenu ({ id, menuHashes }) {
    return httpClient.get(`/_all/menu/mobile.${id}.json?nocache=${menuHashes}`)
  },

  /**
   * Сохранение претензии к заказу
   * @param { Object } data
   * @returns { Promise }
   */
  sendClaim (data) {
    const formData = new FormData()
    for (const [key, value] of Object.entries(data)) {
      if (value || value === 0) formData.append(key, value)
    }
    return httpClient.post('/cgi-bin/my_helper_post.pl?q=save_claim', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
  }
})

export default myHelper
