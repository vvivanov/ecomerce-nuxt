const myUtil = httpClient => ({
  endPoint: '/cgi-bin/my_util2.pl',

  /**
   * Запрос данные пользователя
   * @returns { Promise }
   */
  getClientData () {
    return httpClient.get(this.endPoint + '?q=get_client_data')
  },

  /**
   * Авторизация пользователя
   * @param { Object } data
   * @returns { Promise }
   */
  loginUser (data) {
    return httpClient.post(this.endPoint + '?q=my_login', data)
  },

  /**
   * Выход пользователя
   * @returns { Promise }
   */
  logoutUser () {
    return httpClient.get(this.endPoint + '?q=my_exit')
  },

  /**
   * Регистрация пользователя
   * @param { Object } data
   * @returns { Promise }
   */
  registrationUser (data) {
    return httpClient.post(this.endPoint + '?q=my_registration', data)
  },

  /**
   * Запрос на изменение пароля
   * @param { Object } data
   * @returns { Promise }
   */
  changePassword (data) {
    return httpClient.post(this.endPoint + '?q=my_password', data)
  },

  /**
   * Изменение пароля
   * @param { Object } data
   * @returns { Promise }
   */
  newPassword (data) {
    return httpClient.post(this.endPoint + '?q=my_new_password', data)
  },

  /**
   * Активировать промо код
   * @param { Object } data
   * @returns { Promise }
   */
  activatePromoCode (data) {
    return httpClient.post(this.endPoint + '?q=my_activate_promo', data)
  },

  /**
   * Получить данные аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  getAccountData (data) {
    const query = Object.keys(data).length > 0 ? '&' + new URLSearchParams(data).toString() : ''
    return httpClient.get(this.endPoint + '?q=my_account' + query)
  },

  /**
   * Обновить данные аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  setAccountData (data) {
    return httpClient.post(this.endPoint + '?q=my_account', data)
  },

  /**
   * Подтвердить E-Mail
   * @param { Object } data
   * @returns { Promise }
   */
  confirmEmail (data = {}) {
    return httpClient.post(this.endPoint + '?q=my_email_confirm', data)
  },

  /**
   * Получить Скидки аккаунта
   * @returns { Promise }
   */
  getDiscounts () {
    return httpClient.get(this.endPoint + '?q=my_discount')
  },

  /**
   * Получить Документы аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  getDocuments (data) {
    return httpClient.get(this.endPoint + '?q=my_documents&' + new URLSearchParams(data).toString())
  },

  /**
   * Получить Настройки аккаунта
   * @returns { Promise }
   */
  getOptions () {
    return httpClient.get(this.endPoint + '?q=my_options')
  },

  /**
   * Обновить Настройки аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  setOptions (data) {
    return httpClient.post(this.endPoint + '?q=my_options', data)
  },

  /**
   * Получить Рассылки аккаунта
   * @returns { Promise }
   */
  getSubscription () {
    return httpClient.get(this.endPoint + '?q=my_subscription')
  },

  /**
   * Обновить Рассылки аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  setSubscription (data) {
    return httpClient.post(this.endPoint + '?q=my_subscription', data)
  },

  /**
   * Обновить Лист ожидания аккаунта
   * @param { Object } data
   * @param { Number } page
   * @returns { Promise }
   */
  setWaitList (data, page = 0) {
    const query = page ? `&page=${page}` : ''
    return httpClient.post(this.endPoint + '?q=my_waitlist' + query, data)
  },

  /**
   * Запрос Лист ожидания аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  getWaitList ({ page, archive }) {
    return httpClient.get(this.endPoint + `?q=my_waitlist&page=${page}&archive=${archive}`)
  },

  /**
   * Регистрация и Подтверждение телефона
   * @param { String } phone
   * @param { String } sms
   * @param { Boolean } check_orders
   * @returns { Promise }
   */
  confirmPhoneApi (phone, sms = '', check_orders = false) {
    const point = sms ? '?q=my_phone_confirmation' : '?q=my_code_for_phone_confirmation'
    const body = {
      phone_code: '7',
      phone
    }

    if (sms) {
      body.code = sms
      check_orders && (body.check_orders = 1)
    }
    return httpClient.post(this.endPoint + point, body)
  },

  /**
   * Получить данные для страницы сертификата
   * @returns { Promise }
   */
  getMyCert () {
    return httpClient.get(this.endPoint + '?q=my_cert')
  },

  /**
   * Запрос на активацию сертификата
   * @param { String } code
   * @returns { Promise }
   */
  setMyCert (code) {
    return httpClient.post(this.endPoint + '?q=my_cert', { code })
  },

  /**
   * Уведомления
   * @param { Object } data
   * @returns { Promise }
   */
  setNotifications (data) {
    return httpClient.post(this.endPoint + '?q=my_notification', data)
  },

  /**
   * Получить Тикет для MaidBox
   * @returns { Promise }
   */
  getMBTicket () {
    return httpClient.get(this.endPoint + '?q=my_mb_ticket')
  },

  /**
   * Получить ссылки авторизации для соцсетей
   * @returns { Promise }
   */
  getSocialsNet () {
    return httpClient.get(this.endPoint + '?q=my_social_media')
  },

  /**
   * ???
   * @returns { Promise }
   */
  setNewOrderDefaults (data) {
    return httpClient.post(this.endPoint + '?q=new_order_defaults', data)
  }
})

export default myUtil
