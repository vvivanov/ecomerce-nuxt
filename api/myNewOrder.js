const myNewOrder = httpClient => ({
  endPoint: '/cgi-bin/my_new_order.pl',

  /**
   * Получение данных от сервера для оформления заказа
   * @returns { Promise }
   */
  getOrderData ({ order_id }) {
    let query = '?is_api=1' // параметр is_api никак не используется, нужен чтобы не было проблем с &view_id

    if (order_id) {
      query += `&order_id=${order_id}`
    }

    return httpClient.get(this.endPoint + query)
  },

  /**
   * Отправляет координаты bbox для получения списка точек
   * @returns { Promise }
   */
  setChangeOrder ({ NElat, NElon, SWlat, SWlon, delivery, order_id }, { partner_id, partner_cart, light = false }) {
    const data = {
      'north-east-lat': NElat,
      'north-east-lon': NElon,
      'south-west-lat': SWlat,
      'south-west-lon': SWlon,
      delivery
    }
    const jsonData = JSON.stringify(data)

    let query = ''
    /** параметр light - не вычислять суммы/условия для каждого способа доставки; */
    if (light) {
      query = query + '&light=helper'
    }
    if (order_id) {
      query = `&order_id=${order_id}`
    } else if (partner_id && partner_cart) {
      query = `&partner_id=${partner_id}&cart=${partner_cart}`
    }

    return httpClient.post(this.endPoint + '?q=change_area' + query, jsonData)
  },

  /**
   * ?
   * @returns { Promise }
   */
  changeLocality (params) {
    const queryParams = new URLSearchParams({
      q: 'change_locality',
      ...params
    })
    return httpClient.get(`${this.endPoint}?${queryParams}`)
  },

  /**
   * ?
   * @returns { Promise }
   */
  calc (params, data) {
    const queryParams = new URLSearchParams({
      q: 'calc',
      ...params
    })
    return httpClient.post(`${this.endPoint}?${queryParams}`, data)
  },

  /**
   * ?
   * @returns { Promise }
   */
  newOrder (params, data) {
    const queryParams = new URLSearchParams({
      q: 'new_order',
      ...params
    })
    return httpClient.post(`${this.endPoint}?${queryParams}`, data)
  }
})

export default myNewOrder
