const myOrder = httpClient => ({
  endPoint: '/cgi-bin/my_order2.pl',

  /**
   * Получить Заказ аккаунта
   * @param { String | Number } id
   * @returns { Promise }
   */
  getMyOrder (id) {
    return httpClient.get(this.endPoint + `?q=order&id=${id}`)
  },

  /**
   * Получить Заказы аккаунта
   * @param { Object } data
   * @returns { Promise }
   */
  getMyOrders ({ page }) {
    return httpClient.get(this.endPoint + `?q=orders&page=${page}`)
  },

  /**
   * Получить данные для формы оплаты
   * @param { Object } data
   * @returns { Promise }
   */
  getPaymentForm (data) {
    return httpClient.get(this.endPoint + '?q=get_payment_form&' + new URLSearchParams(data).toString())
  },

  /**
   * Обновить состав заказа
   * @param { Number } orderId
   * @param { Object } data
   * @returns { Promise }
   */
  setChangeOrder (orderId, data) {
    return httpClient.post(this.endPoint + `?q=composition_change&id=${orderId}`, data)
  },

  /**
   * Аннулировать заказ
   * @param { Number } orderId
   * @returns { Promise }
   */
  setCancelOrder (orderId) {
    return httpClient.post(this.endPoint + `?q=order_cancellation&id=${orderId}`, {})
  },

  /**
   * Обновить дату доставки
   * @param { Number } orderId
   * @param { Object } data
   * @returns { Promise }
   */
  setSaveFdDate (orderId, data) {
    return httpClient.post(this.endPoint + `?q=update_fd&id=${orderId}`, data)
  },

  /**
   * Скопировать товары в корзину
   * @param { Number } orderId
   * @param { Object } data
   * @returns { Promise }
   */
  copyProductsToCart (orderId, data) {
    return httpClient.post(this.endPoint + `?q=madd_cart&id=${orderId}`, data)
  }
})

export default myOrder
