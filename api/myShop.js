const myShop = httpClient => ({
  endPoint: '/cgi-bin/shop2.pl',

  /**
   * Получение данных от сервера для инициализации приложения
   * @returns { Promise }
   */
  getInitData () {
    return httpClient.get(this.endPoint + '?q=get_init_data')
  },

  /**
   * Запрос Главной страницы
   * @returns { Promise }
   */
  getMain () {
    return httpClient.get(this.endPoint + '?q=page&id=0')
  },

  /**
   * Запрос Myshop.Гид
   * @param { Object } data
   * @returns { Promise }
   */
  getGuide ({ id, page }) {
    return httpClient.get(this.endPoint + `?q=guide&id=${id}&page=${page}`)
  },

  /**
   * Запрос Справочная система
   * @param { String } data
   * @returns { Promise }
   */
  getHelper (data) {
    return httpClient.get(this.endPoint + '?q=helper&' + data)
  },

  /**
   * Запрос Товары производителя
   * @param { Number } id
   * @returns { Promise }
   */
  getProducers (id) {
    return httpClient.get(this.endPoint + `?q=brands&id=${id}`)
  },

  /**
   * Запрос Отзовы товара
   * @param { Number } id
   * @returns { Promise }
   */
  getReviews (id) {
    return httpClient.get(this.endPoint + `?q=reviews&id=${id}`)
  },

  /**
   * Запрос Акции Myshop
   * @param { String | Number } id
   * @returns { Promise }
   */
  getPromo (id) {
    const query = id ? `&id=${id}` : ''
    return httpClient.get(this.endPoint + `?q=promo${query}`)
  },

  /**
   * Запрос Рекомендации для Страницы товара / Корзины
   * @param { Number } id
   * @returns { Promise }
   */
  getRecommends (id = null) {
    const query = id ? `?q=recommends&product_id=${id}` : '?q=recommends&type=cart'
    return httpClient.get(this.endPoint + query)
  },

  /**
   * Запрос Данные Корзины
   * @returns { Promise }
   */
  getCart () {
    return httpClient.get(this.endPoint + '?q=cart')
  },

  /**
   * Обновить Количество товара
   * @param { Object } data
   * @returns { Promise }
   */
  setCart ({ quantity, productId }) {
    return httpClient.get(this.endPoint + `?q=set_cart&product_id=${productId}&quantity=${quantity}`)
  },

  /**
   * Добавить товар в корзину
   * @param { Object } data
   * @returns { Promise }
   */
  addCart ({ quantity, productId, extra }) {
    return httpClient.get(this.endPoint + `?q=add_cart&quantity=${quantity}&product_id=${productId}${extra}`)
  },

  /**
   * Запрос Добавить товар(ы) в корзину из Избранных
   * @param { String | Number } id
   * @returns { Promise }
   */
  saveCart (id = '') {
    const query = id ? `?q=save_product&product_id=${id}` : '?q=save_products'
    return httpClient.get(this.endPoint + query)
  },

  /**
   * Запрос Вернуть товар(ы) в Корзину
   * @param { Number } id
   * @returns { Promise }
   */
  restoreCart (id = null) {
    const query = id ? `?q=restore_product&product_id=${id}` : '?q=restore_products'
    return httpClient.get(this.endPoint + query)
  },

  /**
   * Запрос Очистить Корзину
   * @returns { Promise }
   */
  deleteCart () {
    return httpClient.get(this.endPoint + '?q=delete_cart_products')
  },

  /**
   * Запрос Очистить Избранное
   * @returns { Promise }
   */
  deleteWishList () {
    return httpClient.get(this.endPoint + '?q=delete_save_products')
  },

  /**
   * Запрос Удалить товар(ы) из Корзины/Избранное
   * @param { String | Number } id
   * @returns { Promise }
   */
  deleteProduct (id = null) {
    return httpClient.get(this.endPoint + `?q=delete_product&product_id=${id}`)
  },

  /**
   * Запрос Данные товара
   * @param { Number } id
   * @returns { Promise }
   */
  getProduct (id) {
    return httpClient.get(this.endPoint + `?q=product&id=${id}`)
  },

  /**
   * Запрос Список товаров
   * @param { Object } data
   * @param { Object } query
   * @returns { Promise }
   */
  getProducts ({ subtype, id, sort, page }, query) {
    return httpClient.get(this.endPoint + `?q=${subtype}&id=${id}&sort=${sort}&page=${page}&` + new URLSearchParams(query).toString())
  },

  /**
   * Запрос Поиск товаров
   * @param { Object } query
   * @returns { Promise }
   */
  searchProducts (query) {
    return httpClient.post(this.endPoint + '?q=search&' + new URLSearchParams(query).toString(), query)
  },

  /**
   * Получить данные капчи
   * @returns { Promise }
   */
  getCaptcha () {
    return httpClient.get(this.endPoint + '?q=captcha')
  },

  /**
   * Партнёры
   * @param { String } query
   *   id - идентификатор партнёра;
   *   product_id - идентификатор товара, не обязательна;
   *   pin - необязательный внутренний числовой код клиента/субпартнера в базе данных партнера:
   * @returns { Promise }
   */
  getPartners (query = '') {
    return httpClient.get(this.endPoint + '?q=partner' + query)
  }
})

export default myShop
