import myShop from './myShop'
import myOrder from './myOrder'
import myNewOrder from './myNewOrder'
import myUtil from './myUtil'
import myHelper from './myHelper'

/**
 * API
 *
 * @module API
 */
export default {
  myShop,
  myNewOrder,
  myOrder,
  myUtil,
  ...myHelper
}
