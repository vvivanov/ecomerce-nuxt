/**
 * Блокируем локальные прямые ссылки в html блоке, остальные пропускаем.
 * @description Директива v-click-prevent-default - отлавливает прямые ссылки и заводит на $router.
 * @example <div v-click-prevent-default v-html="..." />
 */
const clickPreventDefault = {
  bind (el, binding, vnode) {
    el.searchLinks = null
    el.clickPreventDefault = function (event, href) {
      event.preventDefault()
      vnode.context.$router.push(href, () => {}, () => {})
    }
    el.eventListener = function (callback) {
      Object.keys(el.searchLinks)
        .forEach(item => {
          const href = el.searchLinks[item].nodeName === 'A' ? el.searchLinks[item].getAttribute('href') : ''
          if (href.startsWith('/shop/') || href.startsWith('/my/')) {
            callback(el.searchLinks[item], href)
          }
        })
    }
  },
  inserted (el) {
    el.searchLinks = el.getElementsByTagName('a')
    el.eventListener((element, href) => {
      element.addEventListener('click', (e) => el.clickPreventDefault(e, href))
    })
  },
  unbind (el) {
    el.eventListener((element, href) => {
      element.removeEventListener('click', (e) => el.clickPreventDefault(e, href))
    })
  }
}

export default clickPreventDefault
