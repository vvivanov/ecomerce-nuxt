/**
 * @description свой аналог директивы v-mask использующее аналогичное ядро что и https://github.com/scleriot/vue-inputmask
 */
export default {
  bind (el, binding) {
    const getInputmask = () => import(/* webpackChunkName: 'inputmask' */'inputmask/lib/inputmask')
    getInputmask().then((Inputmask) => Inputmask.default(binding.value).mask(el))
  }
}
