const isProduction = process.env.NODE_ENV === 'production';
const MYSHOP_URL = 'https://my-shop.ru';
const MYSHOP_URL_AUTH = process.env.MYSHOP_URL_AUTH;

const getAxiosBaseURL = () => {
  if (isProduction) return MYSHOP_URL;
}

const getAxiosHeaders = () => {
  if (isProduction && MYSHOP_URL_AUTH) return {
    common: {
      authorization: `Basic ${Buffer.from(MYSHOP_URL_AUTH).toString('base64')}`
    }
  }
}

export default {
  ssr: true,
  target: 'server',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'My-shop.ru - Интернет-магазин: книги, учебники, игрушки, канцтовары, подарки, видео, музыка, софт и др. товары',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon-apple-touch-icon.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/favicon-safari-pinned-tab.svg', color: '#f6f6f6' },
      { rel: 'msapplication-TileColor', content: '#f6f6f6' },
      { rel: 'theme-color', content: '#783cbd' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        type: 'text/javascript',
        innerHTML: 'var dataLayer = []; var mindbox = window.mindbox || function() { mindbox.queue.push(arguments); }; mindbox.queue = mindbox.queue || []; mindbox("create", { endpointId: "myshop-website" });',
      },
      {
        type: 'text/javascript',
        innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start": new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?"&l="+l:"";j.async=true;j.src="https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);})(window,document,"script","dataLayer","GTM-WB536KP");`,
      },
      {
        src: 'https://api.mindbox.ru/scripts/v1/tracker.js',
        async: true
      }
    ],
    noscript: [
      {
        innerHTML: '<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB536KP"  height="0" width="0" style="display:none;visibility:hidden"></iframe>',
        pbody: true
      }
    ],
    __dangerouslyDisableSanitizers: ['script', 'noscript']
  },

  loading: {
    color: '#783CBD',
    height: '5px'
  },

  serverMiddleware: ['~/middleware-server/decodeURI'],

  router: {
    middleware: [
      'vue-page-view',
      'check-url-params',
      'mobile-auth'
    ],
    prefetchLinks: false,
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'login',
        path: '/my/homepage',
        component: resolve(__dirname, 'page/Authorization')
      })
      routes.push({
        name: 'registration',
        path: '/my/registration',
        component: resolve(__dirname, 'page/Authorization')
      })
      routes.push({
        name: 'password',
        path: '/my/password',
        component: resolve(__dirname, 'page/Authorization')
      })

      routes.push({
        name: 'cart',
        path: '/my/cart',
        component: resolve(__dirname, 'page/Cart'),
        pathToRegexpOptions: {
          sensitive: true,
          strict: true
        }
      })
      routes.push({
        name: 'cart-save',
        path: '/my/cart/save',
        component: resolve(__dirname, 'page/Cart'),
        pathToRegexpOptions: {
          sensitive: true,
          strict: true
        }
      })
      routes.push({
        name: 'cart-old',
        path: '/my/cart/:a(save|restore|delete)_product_:n(\\d+)',
        component: resolve(__dirname, 'page/Cart')
      })

      routes.push({
        name: 'prod-view',
        path: '/shop/product/:id(\\d+).html',
        component: resolve(__dirname, 'page/ProductView'),
        pathToRegexpOptions: {
          sensitive: true,
          strict: true
        }
      })
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    './assets/scss/_main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/filters' },
    { src: '~/plugins/app-components' },
    { src: '~/plugins/api' },
    { src: '~/plugins/gtm' },
    { src: '~/plugins/api-errors-handler' },
    { src: '~/plugins/app-mixins', mode: 'client' },
    { src: '~/plugins/postscribe', mode: 'client' }, // only on client side
    { src: '~/plugins/directives', mode: 'client' },
    // { src: '~/plugins/axios-logger' } // only on server side
    { src: '~/plugins/global-state' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/style-resources',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    // '@nuxtjs/stylelint-module',
    '@nuxtjs/device',
  ],

  device: {
    refreshOnResize: true // refresh flags when the window resized
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '~/modules/healthcheck',
    // https://github.com/nuxt-community/redirect-module
    '@nuxtjs/redirect-module',
    '@nuxtjs/sentry',
    // '@nuxtjs/gtm',
    'cookie-universal-nuxt',
    /**
     * Use `@nuxtjs/proxy` module only for development
     * for proxy api and other requests on same domain
     */
    () => {
      if (!isProduction) return '@nuxtjs/proxy'
    },
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: getAxiosBaseURL(),
    proxyHeaders: true,
    proxy: !isProduction,
    credentials: true,
    ...(!process.env.LOCAL && { headers: getAxiosHeaders() }),
  },

  proxy: {
    '/_all': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
    },
    '/cgi-bin': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
      changeOrigin: true,
      cookieDomainRewrite: {
        '*': 'localhost',
      },
    },
    '/_files': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
    },
    '/my/partnership': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
    },
    '/help/*.doc': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
    },
    '/favicon*': {
      target: MYSHOP_URL,
      secure: false,
      auth: MYSHOP_URL_AUTH,
    }
  },

  redirect: [
    { from: '/shop/(books|audio|video|soft|health|toys|products|others)/([0-9]+).html', to: '/shop/product/$2.html', statusCode: 301 },
  ],

  sentry: {
    dsn: 'http://359beabdc04849b6aca64d55e24078cd@10.127.10.87:9000/2',
    config: {
      environment: process.env.NODE_ENV
    },
  },

  styleResources: {
    scss: [
      './assets/scss/_variables.scss',
      './assets/scss/_mixins.scss'
    ],
    hoistUseStatements: true,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  },


  devtools: !isProduction,

  messages: {
    error_404: 'В адресе есть ошибка или страница удалена',
  }
}
