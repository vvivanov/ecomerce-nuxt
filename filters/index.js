import Vue from 'vue'
import { MONTHS } from '@/helpers/util'

const WDAYS_SHORT_DOT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']

// 2020-05-01 -> 01.05.20 (Пт.)
Vue.filter('formatDate', (dateStr) => {
  const date = new Date(dateStr)
  let dd = date.getDate()
  if (dd < 10) dd = '0' + dd
  let mm = date.getMonth() + 1
  if (mm < 10) mm = '0' + mm
  const yy = date.getFullYear()
  const wd = date.getDay()
  return dd + '.' + mm + '.' + yy + ' (' + WDAYS_SHORT_DOT[wd] + '.)'
})

// "1937-11-02" -> 2 ноября 1937 г.
// "1937-00-00" -> 1937 г.
Vue.filter('formatDate3', (dateStr) => {
  if (/\s/.test(dateStr)) {
    dateStr = dateStr.split(/\s+/)[0]
  }
  if (/-00/.test(dateStr)) {
    const yy = dateStr.split(/-/)[0]
    return /\d{4}/.test(yy) ? yy + ' г.' : 'неизвестна'
  }
  const date = new Date(dateStr)
  const dd = date.getDate()
  const mm = MONTHS[date.getMonth()]
  const yy = date.getFullYear()
  return dd + ' ' + mm + ' ' + yy + ' г.'
})

// 2020-05-01 -> 1 мая (Пт)
Vue.filter('formatDate4', (dateStr) => {
  const date = new Date(dateStr)
  const dd = date.getDate()
  const wd = date.getDay()
  return dd + ' ' + MONTHS[date.getMonth()] + ' (' + WDAYS_SHORT_DOT[wd] + ')'
})

// "10000" -> 10 000
// 23043302 -> 23 043 302
Vue.filter('localeString', (data) => {
  return data || Number(data) === 0 ? Number(data).toLocaleString('ru-RU') : ''
})
