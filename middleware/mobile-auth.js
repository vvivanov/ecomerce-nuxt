export default function ({ store, route, redirect, $device }) {
  const isMobileAppLoginPage = route.path === '/my/app'
  const isIos = $device.isIos
  const isMobileApp = store.state.is_app_mobile
  const isLogin = store.getters.isLogin

  if (!isMobileAppLoginPage && !isIos && isMobileApp && !isLogin) {
    redirect({ path: '/my/app' })
  }
}
