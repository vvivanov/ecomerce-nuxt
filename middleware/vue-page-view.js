export default function ({ store, route }) {
  if (process.client) {
    store.dispatch('vuePageView', route)
  }
}
