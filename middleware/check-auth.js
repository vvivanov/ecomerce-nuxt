export default function ({ store, route, redirect }) {
  if (!store.getters.isLogin) {
    redirect({ name: 'login', params: { redirect: route.fullPath } })
  }
}
