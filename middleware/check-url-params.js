export default function ({ app, store, query }) {
  const getAppParam = app.$cookies.get('is_app_mobile')
  if (getAppParam) {
    store.commit('setAppMobile', getAppParam)
  } else if (query.app) {
    const expDate = new Date()
    app.$cookies.set('is_app_mobile', query.app, {
      maxAge: expDate.getTime() + 365 * 24 * 60 * 60 * 1000
    })
    store.commit('setAppMobile', query.app)
  }
}
