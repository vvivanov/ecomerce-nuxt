import Vue from 'vue'
import { ObserveVisibility } from 'vue-observe-visibility'
import clickOutside from '@/directives/clickOutside'
import clickPreventDefault from '@/directives/clickPreventDefault'
import inputMask from '@/directives/input-mask'

Vue.directive('click-outside', clickOutside)
Vue.directive('click-prevent-default', clickPreventDefault)
Vue.directive('mask', inputMask);
Vue.directive('observe-visibility', ObserveVisibility)
