import Vue from 'vue'

if (!Vue.__my_mixin__) {
  Vue.__my_mixin__ = true
  Vue.mixin({
    methods: {
      $pageScrollUp (top = 0) {
        window.scroll({
          top,
          behavior: 'smooth'
        })
      },
      $addEventListeners (type, listener) {
        window.addEventListener(type, listener, { passive: true })
        this.$once('hook:destroyed', () => {
          window.removeEventListener(type, listener, { passive: true })
        })
      }
    }
  })
}
