import consola from 'consola';

export default function ({ $axios, redirect, error: nuxtError }) {
  $axios.onResponseError(error => {
    switch (error.response.status) {
      case 301:
        redirect(error.response.data.redirect)
        break
      case 302:
        redirect(error.response.data.redirect)
        break
      case 400:
        nuxtError({
          statusCode: 400,
          title: 'Ошибка',
          message: error.response.data.message || 'Некорректный запрос'
        })
        break
      case 401:
        redirect({ name: 'login', params: { redirect: document.location.href } })
        break
      case 404:
        nuxtError({
          statusCode: 404,
          title: 'Страница не найдена',
          message: error.response.data.message || 'В адресе есть ошибка или страница удалена'
        })
        break
      case 500:
        document.location = '/pause.html'
        break
    }
    consola.error(error.response.data);
    return error
  })
}
