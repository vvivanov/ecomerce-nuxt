import myHelper from '@/api/myHelper'
import myNewOrder from '@/api/myNewOrder'
import myOrder from '@/api/myOrder'
import myShop from '@/api/myShop'
import myUtil from '@/api/myUtil'

export default ({ $axios, store }, inject) => {
  const _axios = $axios.create({
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  })

  _axios.interceptors.request.use(async config => {
    const id = await store.dispatch('getViewId')
    const urlQuery = store.state.is_app_mobile ? `&app=${store.state.is_app_mobile}` : ''
    config.url = config.url + `&view_id=${id}` + urlQuery
    // $sentry.captureMessage('config', 'debug');
    // $sentry.captureException(config, {
    //   tags: {
    //     section: 'task_98',
    //   },
    // })
    return config
  }, error => {
    return Promise.reject(error)
  })

  // _axios.interceptors.response.use(response => {
  //   const { cookie } = response.config.headers
  //   console.log('response:', cookie)
  //
  //   // $sentry.withScope(scope => {
  //   //   scope.setLevel('info')
  //   //   $sentry.captureException('info') // message
  //   // })
  //   if (cookie) {
  //     $sentry.captureMessage('cookie', 'debug') // message и level
  //     $sentry.captureException(cookie, {
  //       tags: {
  //         section: 'task_98',
  //       },
  //     })
  //   }
  //
  //   return response
  // }, error => {
  //   return Promise.reject(error)
  // })

  // Initialize API factories
  const factories = {
    myNewOrder: myNewOrder(_axios),
    myOrder: myOrder(_axios),
    myShop: myShop(_axios),
    myUtil: myUtil(_axios),
    ...myHelper(_axios)
  };

  // Inject $api
  inject('api', factories);
}
