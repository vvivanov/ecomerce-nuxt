// [START log_event]
function logEvent (name, params) {
  if (!name) {
    return
  }

  if (process.client && window.AnalyticsWebInterface) {
    // Call Android interface
    window.AnalyticsWebInterface.logEvent(name, JSON.stringify(params))
  } else if (process.client && window.webkit &&
    window.webkit.messageHandlers &&
    window.webkit.messageHandlers.firebase) {
    // Call iOS interface
    const message = {
      command: 'logEvent',
      name,
      parameters: params
    }
    process.client && window.webkit.messageHandlers.firebase.postMessage(message)
    // } else {
    //   // No Android or iOS interface found
    //   console.log('No native APIs found.')
  }
}
// [END log_event]

const dataPush = data => {
  if (process.client) {
    window.dataLayer.push(data)
  }
}

const mindBox = data => {
  if (process.client) {
    window.mindbox('async', data)
  }
}

export default (ctx, inject) => {
  // $gtm.init('GTM-WB536KP')

  // Initialize API factories
  const factories = {
    namedEvent (name) {
      dataPush({
        event: name
      })

      logEvent(name, {})
    },

    ecomDetail (items) {
      dataPush({
        event: 'ecomDetail',
        ecommerce: {
          detail: {
            products: items
          }
        }
      })

      logEvent('ecomDetail', {
        ecommerce: {
          detail: {
            products: items
          }
        }
      })
    },

    ecomList (items, list) {
      const size = 15
      let sub_ga_listpreview = []

      items.forEach((item, i) => {
        item.position = i + 1
        item.list = list

        sub_ga_listpreview.push(item)

        if (sub_ga_listpreview.length === size || items.length - 1 === i) {
          dataPush({
            event: 'ecomList',
            ecommerce: {
              impressions: sub_ga_listpreview
            }
          })

          logEvent('ecomList', {
            ecommerce: {
              impressions: sub_ga_listpreview
            }
          })

          sub_ga_listpreview = []
        }
      })
    },

    ecomClick (items, list, index) {
      if (index) {
        items[0].position = index + 1
      }

      dataPush({
        event: 'ecomClick',
        ecommerce: {
          click: {
            actionField: {
              list
            },
            products: items
          }
        }
      })

      logEvent('ecomClick', {
        ecommerce: {
          click: {
            actionField: {
              list
            },
            products: items
          }
        }
      })
    },

    ecomCart (items, cart) {
      const products = items.map(item => {
        const prod = { ...item }
        cart.cart.forEach(pos => {
          if (pos.product_id === prod.id) {
            prod.quantity = pos.quantity
          }
        })
        return prod
      })

      dataPush({
        event: 'ecomCart',
        ecommerce: {
          checkout: {
            actionField: {
              step: 1
            },
            products
          }
        }
      })

      logEvent('ecomCart', {
        ecommerce: {
          checkout: {
            actionField: {
              step: 1
            },
            products
          }
        }
      })
    },

    ecomAdd (product, list = '', quantity = 1) {
      product.quantity = quantity

      const ecommerce = {
        add: {
          actionField: {
            list
          },
          products: [product]
        }
      }

      dataPush({
        event: 'ecomAdd',
        ecommerce
      })

      logEvent('ecomAdd', {
        ecommerce
      })
    },

    ecomRemove (items, quantity) {
      items[0].quantity = quantity

      dataPush({
        event: 'ecomRemove',
        ecommerce: {
          remove: {
            products: items
          }
        }
      })

      logEvent('ecomRemove', {
        ecommerce: {
          remove: {
            products: items
          }
        }
      })
    },

    ecomCheckout (items) {
      dataPush({
        event: 'ecomCheckout',
        ecommerce: {
          checkout: {
            actionField: {
              step: 2
            },
            products: items
          }
        }
      })

      logEvent('ecomCheckout', {
        ecommerce: {
          checkout: {
            actionField: {
              step: 2
            },
            products: items
          }
        }
      })
    },

    ecomPurchase (items, info) {
      const lines = items.map(item => ({
        basePricePerItem: String(item.price), // "<Базовая цена продукта за единицу продукта>",
        quantity: String(item.quantity), // "<Количество SKU (или продукта, если SKU не передан)>",
        product: {
          ids: {
            website: String(item.id) // "<Id Product в Сайт>"
          }
        }
      }))

      const mindBoxData = {
        customer: {
          ids: {
            websiteID: String(info.client_id) // "<User ID>"
          }
        },
        order: {
          ids: {
            websiteID: String(info.order_id) // "<Идентификатор заказа Идентификатор заказа на сайте>"
          },
          deliveryCost: String(info.sum_delivery), // "<Стоимость доставки заказа>",
          customFields: {
            deliveryType: String(info.delivery_type) // "<Способ доставки>"
          },
          totalPrice: String(info.sum_total), // "<Итоговая сумма, полученная от клиента. Должна учитывать возвраты и отмены. Используется для подсчета среднего чека.>",
          lines
        }
      }

      dataPush({
        event: 'ecomPurchase',
        ecommerce: {
          purchase: {
            actionField: {
              id: info.order_id,
              revenue: info.sum_total,
              shipping: info.sum_delivery,
              affiliation: 'www.my-shop.ru',
              coupon: info.promo_code
            },
            products: items,
            mindbox: mindBoxData
          }
        }
      })

      logEvent('ecomPurchase', {
        ecommerce: {
          purchase: {
            actionField: {
              id: info.order_id,
              revenue: info.sum_total,
              shipping: info.sum_delivery,
              affiliation: 'www.my-shop.ru',
              coupon: info.promo_code
            },
            products: items
          }
        }
      })

      mindBox({
        operation: 'Website.CreateUnauthorizedOrder',
        data: mindBoxData
      })
    },

    wishlistAdd (items) {
      dataPush({
        event: 'wishlistAdd',
        ecommerce: {
          add_wishlist: {
            products: items
          }
        }
      })

      logEvent('wishlistAdd', {
        ecommerce: {
          add_wishlist: {
            products: items
          }
        }
      })
    },

    autoEvent () {
      dataPush({
        event: 'autoEvent',
        eventCategory: 'registration',
        eventAction: 'passed',
        eventLabel: ''
      })

      logEvent('autoEvent', {
        eventCategory: 'registration',
        eventAction: 'passed',
        eventLabel: ''
      })
    },

    vuePageView (type, breadcrumbs, user_id, originLocation, url, title) {
      // if ('google_tag_manager' in window) {
      //   const gtm = window.google_tag_manager['GTM-WB536KP']
      //   gtm.dataLayer.reset()
      // }
      const pageType = this.getPageType(type)

      const item = {
        event: 'vuePageView',
        pagePath: url,
        pageType,
        pageTitle: title,
        originalLocation: originLocation
      }

      if (user_id) {
        item.userId = user_id
      }
      if (breadcrumbs) {
        item.categoryId = breadcrumbs
      }

      dataPush(item)

      logEvent('vuePageView', {
        ...item
      })
    },

    typePage (href, user_id, originLocation) {
      if (href.match(/\/shop\/product\//) || href.match(/\/shop\/catalogue\//)) {
        const id = href.match(/\/shop\/(product)\/?(\d*)(?:.html|)/) || href.match(/\/shop\/(catalogue)\/?(\d*)\//)
        if (id && id[1] && id[2]) {
          fetch(window.initialData.url_lggr_ext + '?q=lex_breadcrumbs&id=' + id, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json;charset=utf-8' }
          })
            .then(data => {
              this.addTypePage(id[1] === 'product' ? 'товар' : 'раздел', data.breadcrumbs || '', user_id, originLocation)
            })
        } else if (id && id[1]) {
          this.addTypePage(id[1] === 'product' ? 'товар' : 'раздел', '', user_id, originLocation)
        } else {
          this.addTypePage(id[1] === '', '', user_id, originLocation)
        }
      } else if (href.match(/\/shop\/article\//)) {
        this.addTypePage('статья гида', '', user_id, originLocation)
      } else if (window.location.href.match(/\/my\/helper/)) {
        this.addTypePage('статья', '', user_id, originLocation)
      } else {
        this.addTypePage('', '', user_id, originLocation)
      }

      window.initialData.startApp = 1
    },

    addTypePage (type, breadcrumbs, user_id, originLocation) {
      const pageType = type ? this.getPageType(type) : ''
      // window.initialData.startApp = 1

      const item = {
        pageType,
        categoryId: breadcrumbs
      }

      if (user_id) {
        item.userId = user_id
      }
      if (originLocation) {
        item.originalLocation = originLocation
      }

      dataPush(item)

      logEvent(pageType, {
        ...item
      })
    },

    googleTagParams (ecomm_prodid, ecomm_pagetype, ecomm_totalvalue) {
      dataPush({
        google_tag_params: {
          ecomm_prodid,
          ecomm_pagetype,
          ecomm_totalvalue
        }
      })
    },

    getPageType (name) {
      let pageType = ''
      switch (name) {
        case 'index':
          pageType = 'home'
          break
        case 'producers':
        case 'prod-list-bytype':
        case 'prod-list-article':
          pageType = 'category'
          break
        case 'prod-view':
          pageType = 'product'
          break
        case 'cart':
        case 'cart-save':
          pageType = 'cart'
          break
        case 'new-order-confirm':
          pageType = 'purchase'
          break
        case 'prod-list-search':
        case 'prod-list-search-mini':
          pageType = 'searchresults'
          break
        default:
          pageType = 'other'
      }
      return pageType
    },

    regEmail ({ lastName, firstName, midleName, email, userId, customerType, isSubscribed }) {
      dataPush({
        event: 'registration',
        lastName, // '<Фамилия>',
        firstName, // '<Имя>',
        midleName, // '<Отчество>',
        email, // '<емейл>',
        userId, // '<UserID>',
        customerType, // '<Вид клиента>',
        isSubscribed // '<Значение подписки, оставленное клиентоа на форме>'
      })

      logEvent('registration', {
        lastName, // '<Фамилия>',
        firstName, // '<Имя>',
        midleName, // '<Отчество>',
        email, // '<емейл>',
        userId, // '<UserID>',
        customerType, // '<Вид клиента>',
        isSubscribed // '<Значение подписки, оставленное клиентоа на форме>'
      })
    },

    regPhone ({ mobilePhone, userId }) {
      dataPush({
        event: 'registration2',
        mobilePhone, // '<Мобильный телефон>',
        userId // '<UserID>',
      })

      logEvent('registration2', {
        mobilePhone, // '<Мобильный телефон>',
        userId // '<UserID>',
      })
    },

    authUser (userID) {
      dataPush({
        event: 'authorization',
        userID // '<UserID>'
      })

      logEvent('authorization', {
        userID
      })
    },

    customerEdit ({ authenticationTicket, birthDate, sex, lastName, firstName, middleName, email, mobilePhone }) {
      dataPush({
        event: 'customeredit',
        authenticationTicket, // '<Тикет аутентификации клиента>',
        birthDate, // '<Дата рождения>',
        sex, // '<Пол>',
        lastName, // '<Фамилия>',
        firstName, // '<Имя>',
        middleName, // '<Отчество>',
        email, // '<Е-мейл>',
        mobilePhone // '<Мобильный телефон>'
      })

      logEvent('customeredit', {
        birthDate, // '<Дата рождения>',
        sex, // '<Пол>',
        lastName, // '<Фамилия>',
        firstName, // '<Имя>',
        middleName, // '<Отчество>',
        email, // '<Е-мейл>',
        mobilePhone // '<Мобильный телефон>'
      })
    }
  };

  // Inject $gtmTrack
  inject('gtmTrack', factories);
}
