import Vue from 'vue'
import VTooltipPlugin from 'v-tooltip'
import BButton from '@/components/ui/Button'
import BClose from '@/components/ui/ButtonClose'
import Img from '@/components/ui/Img'

Vue.component('BButton', BButton)
Vue.component('BClose', BClose)
Vue.component('Img', Img)

Vue.use(VTooltipPlugin)
