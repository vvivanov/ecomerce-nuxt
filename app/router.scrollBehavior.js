export default function (to, from, savedPosition) {
  if (to.hash) {
    const hash = to.hash.replace(/#/, '')
    const element = document.getElementById(hash) || document.querySelector(`[name='${hash}']`)
    if (element && element.scrollTo) {
      const { top } = element.getBoundingClientRect()
      const width = document.documentElement.clientWidth
      window.scrollTo({
        top: width < 768 ? top : top - 90,
        behavior: 'smooth'
      })
    }

    return { el: to.hash }
  } else if (savedPosition) {
    return savedPosition
  } else {
    return { x: 0, y: 0 }
  }
}
