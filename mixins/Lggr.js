import { logger } from '@/helpers/util'

export default {
  created () {
    // logger('created', '', {buffer: 1});
  },
  mounted () {
    this.$nextTick(() => {
      logger('mounted', '', { use_buffer: 1 })
    })
  },
  beforeDestroy () {
    // logger('before-destroy', '', {use_buffer: 1});
  },
  destroyed () {
    logger('destroyed', '', { use_buffer: 1 })
  },
  updated () {
    logger('updated', '', { use_buffer: 1 })
  }
}
