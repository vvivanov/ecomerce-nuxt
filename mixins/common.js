import { mapState, mapMutations } from 'vuex'
import { getWindowWidth } from '@/helpers/util'

export default {
  computed: {
    ...mapState('modals', { modalName: 'name', modalShow: 'status' }),
  },
  mounted () {
    this.onResize()

    window.onpopstate = () => {
      !this.modalName && this.modalShow && this.toggleModal({ goBack: false })
    }

    this.$addEventListeners('resize', this.onResize)
  },
  methods: {
    ...mapMutations(['setWindowWidth', 'setIsMobile']),

    onResize () {
      this.setIsMobile(window.innerWidth < 768)
      this.setWindowWidth(getWindowWidth())
    },
  }
}
