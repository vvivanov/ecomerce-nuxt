export default {
  props: {
    value: {
      type: String | Number,
      default: ''
    },
    type: {
      type: String,
      default: 'text'
    },
    label: {
      type: String,
      default: ''
    },
    name: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: ' '
    },
    required: {
      type: Boolean,
      default: false
    },
    validations: {
      type: Boolean,
      default: null
    },
    readonly: {
      type: [String, Boolean],
      default: false
    },
    message: {
      type: String,
      default: ''
    },
    info: {
      type: String,
      default: ''
    },
    maxlength: {
      default: false
    },
    rules: {
      type: Array,
      default: () => []
    },
    autocomplete: {
      type: String,
      default: 'on'
    }
  },
  data () {
    return {
      valid: null,
      error: '',
      prevInputState: ''// состояние из инпута для перехвата keydown
    }
  },
  computed: {
    errorMessage () {
      return this.error || this.message
    }
  },
  watch: {
    value: {
      handler (newVal, oldVal) {
        if (newVal === oldVal) return
        this.validate()
      },
      deep: true
    },
    validations (val) {
      val === false ? this.valid = false : this.validate()
    }
  },
  methods: {
    validate () {
      const value = this.value
      const errors = []

      this.valid = value === null ? null : !!value

      if (this.rules.length > 0) {
        this.rules.forEach(rule => {
          const valid = typeof rule === 'function' ? rule(value) : rule

          if (valid === false || typeof valid === 'string') {
            errors.push(valid || '')
            this.error = valid
          }
        })

        this.valid = errors.length === 0
      }

      return this.valid
    },
    inputHandler (e) {
      if (e.ctrlKey && e.keyCode === 90) { // комбинация ctrl + z присвоем последнее запомненое значение
        // e.preventDefault() //почему-то не срабатывает
        e.target.value = this.prevInputState
        e.target.focus()// оставим фокус
      } else if (e.ctrlKey && e.keyCode === 86) { // комбинация ctrl + v не подхватится поэтому запишем значение вручную
        setTimeout(() => {
          this.$emit('input', e.target.value.trim())
          this.prevInputState = e.target.value.trim()
        }, 200)
      } else if (e.keyCode !== 8 && e.keyCode !== 46) { // исключая backspace & delete
        // после нажатия в инпуте не будет последнего символа
        // обновим с небольшой задержкой чтобы успел подхватить последний символ
        setTimeout(() => {
          // this.$emit('input', e.target.value.trim())
          this.prevInputState = e.target.value.trim() // запишем данные из инпута
        }, 200)
      }
    }
  }
}
