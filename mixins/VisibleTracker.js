export default {
  data () {
    return {
      elementVisible: false
    }
  },
  mounted () {
    this.vinterval = setInterval(() => this.trackVisibility(), 400)
  },
  beforeDestroy () {
    clearInterval(this.vinterval)
  },
  methods: {
    trackVisibility () {
      this.elementVisible = this.$el.offsetParent !== null
    }
  }
}
