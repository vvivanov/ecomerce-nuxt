/**
 * @memberof Store.modals
 * @constant state
 * @description хранилище для работы с модальными окнами
 * @example <caption>Условие для отображения</caption>
 * this.modalName === 'имя модального окна'
 * @example <caption>Вызвать</caption>
 * this.toggleModal({ name: 'имя модального окна' })
 * @example <caption>Закрыть</caption>
 * this.toggleModal({ name: null })
 * @description обработка кнопки Escape и Назад на мобильных, вертикального скролла происходит тут
 */

export const state = () => ({
  status: false,
  name: null,
  props: {}
})

export const mutations = {
  set (state, { props }) {
    state.props = props
  },

  toggle (state, payload) {
    state.status = payload
  },

  setName (state, name) {
    state.name = name
  },

  setStatus (state, payload) {
    state.status = payload
  }
}

export const actions = {
  /**
 * @param {string | null} [name = null] - название попапа
 * @param {boolean} [goBack = true] - нужен ли возврат по истории, false используется только из события onpopstate
 * @param {boolean} [hideScrollOnDesktop = true] - нужно ли убирать скроллинг страницы на десктопе
 * @param {boolean} [hideScrollOnMobile = true] - нужно ли убирать скроллинг страницы на мобильной
 */
  toggle ({ state, commit, dispatch }, { name = null, goBack = true, hideScrollOnDesktop = true, hideScrollOnMobile = true }) {
    const isMobile = !window.matchMedia('(min-width: 768px)').matches
    const isHash = document.location.hash === '#modal' // делаем переход по истории только если в адрессной строке есть #modal, т.е. есть дополнительная запись истории
    const onKeyup = ({ key }) => {
      dispatch('escEvent', { key })
    }

    if (state.status) {
      /**
       * FIXME: ломает авторизацию. После нажатия "Войти по паролю" закрывает модальное окно
       */
      // commit('toggle', false)

      isMobile && goBack && isHash && window.history.go(-1) // когда скрываем попап, переходим назад по истории на текущую страницу

      if (document.body.style.overflow === 'hidden') document.body.style.overflow = ''

      window.removeEventListener('keyup', onKeyup)
    } else if (name !== null) { // даже не заходим сюда если нет имени
      commit('toggle', true)

      isMobile && window.history.pushState(null, null, '#modal') // когда показываем попап, пишем в историю пустой переход

      if ((hideScrollOnDesktop && !isMobile) || (hideScrollOnMobile && isMobile)) document.body.style.overflow = 'hidden'

      window.addEventListener('keyup', onKeyup, { once: true })
    }

    // если состояние попапа false, то сбрасываем его имя, нужно для работы режима toggle
    commit('setName', state.status ? name : null)
  },

  escEvent ({ dispatch }, { key }) {
    if (key === 'Escape') {
      dispatch('toggle', { name: null })
    }
  }
}
