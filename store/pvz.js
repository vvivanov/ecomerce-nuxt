import Vue from 'vue'

/**
 * @memberof Store.pvz
 * @constant state
 * @type {Object}
 * @description хранилище для работы с состояниями ПВЗ а также некоторая информация для работы с картой выбора ПВЗ
 * @property {object{}} contractors - ссылка на хэш со всеми службами доставки: {5 => {name => 'Почта России', sort => 40}, ...}
 * @property {string} selectedDeliveryId -  уникальный код доставки
 * @property {object} defaultLocality - информация о населённом пункте и доступных способах доставки/оплаты, см. описание MyShop::Order::locality()
 * @property {object} currentLocality - информация о населённом пункте и доступных способах доставки/оплаты, см. описание MyShop::Order::locality()
 * @property {object} selectedDelivery - инф-я о выбранной доставке
 * @property {array} pvz - активный список ПВЗ
 * @property {boolean} showUnavailDlv - показывать/нет недоступные доставки
 */

export const state = () => ({
  selectedDeliveryId: null,
  defaultLocality: {},
  currentLocality: {},
  selectedDelivery: null,
  pvz: [],
  contractors: {},
  showUnavailDlv: false
})

/**
 * @memberof Store.pvz
 * @constant getters
 * @type {Object}
 * @property {Boolean} currierSelected - св-во указывает выбрана ли курьерская доставка
 * @property {Boolean} PvzSelected -  св-во указывает выбрана ли доставка в ПВЗ
 * @description вычислсяемые св-ва храниища
 */
export const getters = {
  currierSelected (state) {
    return state.selectedDeliveryId ? /^9/.test(state.selectedDeliveryId) : false
  },
  PvzSelected (state) {
    return state.selectedDeliveryId ? /^8/.test(state.selectedDeliveryId) : false
  },

  /**
   * Формируем список ПВЗ
   * @param {Object{}} locality сырые данные с бека
   * @param {Boolean} showUnavailDlv показать/скрыть недоступные способы доставки
   * @param {Object{}} contractors список доступных способов доставки
   * @returns {Object[]} список ПВЗ
   */
  pvz (state) {
    if (!state.currentLocality.d8) return []

    return state.currentLocality.d8
      .filter(e => state.showUnavailDlv ? true : e.av !== 0)
      .map(e => {
        const l = state.currentLocality[e.id]
        const c = state.contractors[l.contractor_id]

        return {
          ...{
            delivery: e.id,
            contractor_name: c ? c.name : '',
            contractor_logo: c ? c.id : '',
            contractor_sort: c ? c.sort : ''
          },
          ...l
        }
      })
  }  
}

export const actions = {
  combineDeliveryAction ({ state, commit }, { delivery }) {
    if (delivery !== null) {
      const deliveryCode = Number(delivery)
      commit('selectedDeliveryIdMutation', deliveryCode)
      let locality = null

      if (deliveryCode in state.currentLocality) {
        locality = state.currentLocality
      } else if (deliveryCode in state.defaultLocality) {
        locality = state.defaultLocality
      } else {
        console.error(`нет данных по выбранному ПВЗ ${deliveryCode} в initialData или $store.state.pvz.currentLocality`) // eslint-disable-line no-console
      }
      const selectedDelivery = locality[deliveryCode]
      if (selectedDelivery) {
        commit('selectedDeliveryMutation', locality[deliveryCode])
      }
    }
  },
  resetMapToDefault ({ state, commit }) {
    commit('currentLocalityMutaion', state.defaultLocality)
  }
}

export const mutations = {
  selectedDeliveryIdMutation (state, payload) {
    state.selectedDeliveryId = payload
  },
  setDefaultLocality (state, payload) {
    state.defaultLocality = payload
  },
  currentLocalityMutaion (state, payload) {
    Vue.set(state, 'currentLocality', payload)
  },
  selectedDeliveryMutation (state, payload) {
    Vue.set(state, 'selectedDelivery', payload)
  },
  resetDeliveryMutation (state) {
    state.selectedDeliveryId = null
    state.selectedDelivery = null
  },
  selectedDeliveryCalcUpdate (state, payload) {
    state.selectedDelivery = { ...state.selectedDelivery, ...payload }
  },
  setContractors (state, payload) {
    state.contractors = payload
  },
  setShowUnavailDlv (state, payload) {
    state.showUnavailDlv = payload
  }  
}
