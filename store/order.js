/**
 * @memberof Store.order
 * @constant state
 * @type {Object}
 * @description хранилище для работы с оформлением заказа
 * @property {boolean} showUnavailDlv - флаг отображания сведений (показывать|не показывать ) недоступные точки
 * @property {array} pvzCollection - коллекция ПВЗ в выбранном НП
 * @property {array} defaults - наборы умолчаний (настроек клиента), см. описание MyShop::Order::defaults()
 * @property {number} orderId - id текущего заказа, если есть
 * @property {boolean} hideDiscount - не показывать скидки и цены/суммы с учётом скидок
 * @property {object} products - информация о товарах заказа
 * @property {object} order - общая информация о заказе
 * @property {array} availableOrder - заказ, в который можно добавить товары вместо оформления нового заказа
 * @property {array} confirmedPhones - подтверждённые клиентом номера телефонов
 */

export const state = () => ({
  showUnavailDlv: false,
  pvzCollection: [],
  defaults: [],
  orderId: null,
  hideDiscount: false,
  products: {},
  order: {},
  availableOrder: [],
  confirmedPhones: []
})

export const mutations = {
  showUnavailDlvMutation (state, payload) {
    state.showUnavailDlv = payload
  },
  pvzCollectionMutation (state, payload) {
    state.pvzCollection = payload
  },
  setDefaults (state, payload) {
    state.defaults = payload
  },
  /**
   * @param {number} orderId - id заказа выбранного адреса
   */
  setDefaultsDelivery (state, { orderId }) {
    state.defaults.forEach(({ info }) => {
      if (!info) return 

      info.main = info.order_id === orderId ? 1 : null
    })
  },
  setOrderId (state, payload) {
    state.orderId = payload
  },
  setHideDiscount (state, payload) {
    state.hideDiscount = payload
  },
  setProducts (state, payload) {
    state.products = payload
  },
  setOrder (state, payload) {
    state.order = payload
  },
  setAvailableOrder (state, payload) {
    state.availableOrder = payload
  },
  addConfirmedPhones (state, payload) {
    state.confirmedPhones.push(payload)
  },
  /**
   * Получаем подтверждённые телефоны
   * @returns {Array.<string>} список подтверждённых номеров с бека
   */
  setConfirmedPhones (state, payload = {}) {
    const confirmedPhones = []

    for (const value in payload) {
      payload[value] && confirmedPhones.push(value.replace('7 ', ''))
    }
  
    state.confirmedPhones = confirmedPhones
  }
}

export const actions = {
  async getOrderData ({ commit }, order_id = '') {
    const { data } = await this.$api.myNewOrder.getOrderData({ order_id })
    const { defaults, contractors, ao, hide_discount, products, order, available_order = [], confirmed_phones, h1, confirm_phone } = data
    let { locality } = data
    
    if (!locality && ao) {
      const { guid: ao_guid, longname:  ao_longname, region_id } = ao
      locality = { ao_guid, ao_longname, region_id, d9: [], d8: [] }
    }

    commit('setDefaults', defaults)
    commit('setOrderId', order_id)
    commit('setHideDiscount', hide_discount)
    commit('setProducts', products)
    commit('setOrder', order)
    commit('setAvailableOrder', available_order)
    commit('setConfirmedPhones', confirmed_phones)

    commit('pvz/setContractors', contractors, { root: true })
    commit('pvz/setDefaultLocality', locality, { root: true })
    commit('pvz/currentLocalityMutaion', locality, { root: true })

    return { h1, confirmPhone: confirm_phone }
  },
  async getHelpreData ({ commit, rootGetters }) {
    const { guid } = rootGetters['application/addressPoint']
    const { data: { locality, contractors } } = await this.$api.setLocality(guid)

    commit('pvz/setContractors', contractors, { root: true })
    commit('pvz/setDefaultLocality', locality, { root: true })
    commit('pvz/currentLocalityMutaion', locality, { root: true })
  },
  sendClaim (_ctx, data) {
    return this.$api.sendClaim(data)
  }
}
