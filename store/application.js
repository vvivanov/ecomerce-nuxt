export const state = () => ({
  initialData: null,
  isLoading: false
})

export const getters = {
  /**
   * Вход выполнен под оператором
   */
  isOperatorMode: state => !!state.initialData.user?.operator_login,
  addressPoint: state => state.initialData?.ao || {}
}

export const mutations = {
  setInitialData (state, payload) {
    state.initialData = payload
  },
  setAddressPoint (state, payload) {
    state.initialData.ao = {
      ...state.initialData.ao,
      ...payload
    }
  },
  setLoading (state, payload) {
    state.isLoading = payload
  }
}

export const actions = {
  async getInitData ({ commit }) {
    const { data: { initData } } = await this.$api.myShop.getInitData()

    commit('updateCartPartial', initData.cart, { root: true })
    commit('setClient', initData.client, { root: true })
    commit('setInitialData', initData)
  }
}

