import Vue from 'vue'
import { pathOr, omit } from 'ramda'
import { getWindowWidth, logger, extractUserFilterData, groupByFld } from '@/helpers/util'

const DEFAULT_PAGE_META = {
  producer: {},
  person: {},
  categories: [],
  ogimage: '',
  keywords: '',
  canonical: '',
  description: '',
  title: ''
}

export const state = () => ({
  is_app_mobile: null,
  isFloatBtn: 24,
  client: {
    id: null,
    name: ''
  },
  isMobile: null,
  windowWidth: getWindowWidth(),
  productListPage: 1,
  showLoader: true,
  ajaxInProgress: false,
  helpHint: null,
  pageMeta: DEFAULT_PAGE_META,
  products: [],
  productsOther: [],
  productsTotal: null,
  errors: [],
  mainData: null,
  productsSort: 'a',
  cartSort: 'a',
  userFilter: {},
  filters: [],
  curProduct: {},
  waitlistAddedN: 0,
  notifications: {},
  cart: {
    total: {
      time: 0,
      quantity: 0
    },
    cart: [],
    save: [],
    cartObj: {},
    saveObj: {}
  },
  cartTotal: null,
  waitListProducts: [],
  viewId: null
})

export const getters = {
  cartTotal: state => state.cart.total ? Number(state.cart.total.quantity) : 0,
  qtyOfProductInCart: state => prod_id => {
    let qty = 0
    for (const p of (state.cart || []).cart) {
      qty += p.product_id === prod_id ? +p.quantity : 0
    }
    return qty
  },
  client: state => state.client,
  isLogin: state => (state.client.id ^ 0) > 0,
  productBreadcrumbs: state => state.pageMeta.path && state.pageMeta.path[0] ? state.pageMeta.path[0].join(',') : '',
  notifications: state => state.notifications
}

export const mutations = {
  /**
   * @param {number} payload
   */
  setFloatBtn (state, payload) {
    state.isFloatBtn = payload
  },
  setClient (state, v) {
    state.client = v
  },
  setClientName (state, name) {
    state.client.name = name
  },
  setHelpHint (state, v) {
    state.helpHint = v // { text, title, href }
  },
  showLoader (state) {
    state.showLoader = true
  },
  hideLoader (state) {
    state.showLoader = false
  },
  setAjaxInProgress (state, v) {
    state.ajaxInProgress = !!v
  },
  productListPageSet (state, v) {
    state.productListPage = v
  },
  setProductsSort (state, s) {
    state.productsSort = s
  },
  setIsMobile (state, payload) {
    state.isMobile = payload
  },
  setWindowWidth (state, payload) {
    state.windowWidth = payload
  },
  updateCart (state, cart) {
    const cartObj = {}
    const saveObj = {}
    for (const o of cart.cart) {
      cartObj[+o.product_id] = +o.quantity
    }
    for (const o of cart.save) {
      saveObj[+o.product_id] = true
    }
    state.cart = { ...cart, cartObj, saveObj }
  },
  updateCartPartial (state, { cart, save, total }) {
    if (total && total.time && total.time >= state.cart.total.time) {
      /* Object.assign(state.cart.total, total)
       * state.cart.cartObj = cart
       * state.cart.saveObj = save */
      state.cart = { ...state.cart, total, cartObj: cart, saveObj: save }
    }
  },
  updateMainData (state, data) {
    state.mainData = data
  },
  updateErrors (state, data) {
    state.errors = data
  },
  updateCartItemQty (state, { productId, type, quantity }) {
    for (const item of state.cart[type]) {
      if (item.product_id === productId) {
        item.quantity = quantity
      }
    }
    state.cart[`${type}Obj`][productId] = quantity
  },
  updateCurProduct (state, data) {
    if (data) {
      state.curProduct = data
    }
    if (data.about || data.characteristics) {
      data.attrs = groupByFld('name', data.about)
      data.attrs12 = groupByFld('name', data.characteristics)
      if (data && data.attrs12) {
        data.attrs1 = []
        data.attrs2 = []
        const l = Math.floor(data.attrs12.length / 2)
        for (let i = 0; i < l; i++) {
          data.attrs1.push(data.attrs12[i])
        }
        for (let i = l; i < data.attrs12.length; i++) {
          data.attrs2.push(data.attrs12[i])
        }
      }
    }
  },
  updateRecommends (state, data) {
    if (data && data.products) {
      Vue.set(state.curProduct, 'blocks', [data])
    }
  },
  updateProducts (state, { products, productsTotal }) {
    state.products = products
    state.productsTotal = productsTotal
  },
  updateProductsOther (state, other) {
    state.productsOther = other
  },
  updateFilters (state, filters) {
    state.filters = filters.map(item => {
      item.values.sort((a, b) => a.top > b.top ? -1 : 1) // сортировка по top

      if (item.name !== 'p38') { // наличие
        item.values.sort((a, b) => a.selected > b.selected ? -1 : 1) // отмеченные фильтры поднимаем вверх
      }

      return item
    })

    // обновить значения для range-фильтра, если они выходят за пределы min,max
    const fs = filters && filters.filter ? filters.filter(x => x.type === 'range') : []
    for (const f of fs) {
      if (f.values && f.values.length === 2) {
        for (const v of f.values) {
          const [fld] = v.name.split('_')
          if (state.userFilter[fld]) state.userFilter[fld][v.name] = v.value
        }
      }
    }
  },
  updateUserFilter (state, data) {
    state.userFilter = Object.assign({}, state.userFilter, data)
  },
  setUserFilter (state, data) {
    state.userFilter = data
  },
  updatePageMeta (state, meta) {
    state.pageMeta = { ...DEFAULT_PAGE_META, ...meta }
  },
  updateWaitlistAdded (state) {
    state.waitlistAddedN += 1
  },
  setBreadcrumbs (state, breadcrumbs) {
    let resultBredcrumbs = ''

    if (breadcrumbs) {
      // breadcrumbs = [ [ '1', '2', '3'], [ '4', '5' ] ]
      resultBredcrumbs = breadcrumbs.map(arr => {
        // возвращает [ {"1":{"2":["3"]}}, {"4":["5"]} ]
        return arr.reduceRight((previousValue, currentValue) => {
          if (!previousValue) {
            return [currentValue]
          }
          return { [currentValue]: previousValue }
        }, false)
      })
    }
    state.breadcrumbs = resultBredcrumbs
  },
  updateNotifications (state, payload) {
    state.notifications = { ...state.notifications, ...payload }
  },
  updateWaitList (state, payload) {
    state.waitListProducts = payload
  },
  addToWaitList (state, id) {
    id = Number(id)
    
    if (!state.waitListProducts.includes(id)) {
      state.waitListProducts.push(id)
    }
  },
  setAppMobile (state, payload) {
    state.is_app_mobile = payload
  },
  setViewId (state, payload) {
    state.viewId = payload
  }
}

export const actions = {
  async loadProducts ({ state, commit }, { subtype, page, sort, id, skipCache, route, query } = {}) {
    subtype = subtype || 'catalogue'
    if (sort) commit('setProductsSort', sort)
    page = page || state.productListPage
    commit('updateProducts', { products: [], productsTotal: null })
    commit('showLoader')

    let newQuery = {
      ...query,
      ...(skipCache ? { skipCache } : {}),
      in_orders: query?.in_orders && (query.in_orders === true || query.in_orders === 'true')
    }
    newQuery = query?.q ? { ...omit(['q'], query), f14_6: query.q } : query

    try {
      const { data } = route === 'prod-list-search'
        ? await this.$api.myShop.searchProducts({ ...newQuery, sort, page })
        : await this.$api.myShop.getProducts({ subtype, id, sort, page }, newQuery)

      if (data) {
        if (data.redirect) {
          this.$router.replace(data.redirect).then()
          return
        }
        if (data.products) commit('productListPageSet', page)
        commit('updatePageMeta', data.meta)
        if (data.meta.sort) commit('setProductsSort', data.meta.sort)
        if (data.filter) {
          commit('updateFilters', data.filter)
          commit('setUserFilter', extractUserFilterData(data.filter))
        }
        const products = data.products || []
        const productsTotal = pathOr(null, ['meta', 'total'], data) // data.meta.total || null
        commit('updateProducts', { products, productsTotal })
        commit('updateMainData', { blocks: data.blocks || [], article: data.article || null, promos: data.promos, banners: data.banners || {}, producer: data.producer || {}, person: data.person || {}, series: data.series || {}, subcategories: data.subcategories || [], also: data.also || {}, id, activated_promo: data.activated_promo || null })
        commit('updateProductsOther', data.other || [])
        if (data.cart) commit('updateCartPartial', data.cart)
        if (data.meta.path) commit('setBreadcrumbs', data.meta.path)
      } else {
        commit('updateProducts', { products: null, productsTotal: null })
      }
    } finally {
      commit('hideLoader')
    }
  },
  loadCart ({ commit }) {
    commit('showLoader')
    return this.$api.myShop.getCart()
      .then(({ data }) => {
        commit('updateCart', data)
        if (data.meta) commit('updatePageMeta', data.meta)
        commit('hideLoader')
        this.$gtmTrack.googleTagParams(data.cart.map(item => item.product_id), 'cart', data.total.cost_discount || data.total.cost)
        const products = data.cart.map(item => item.ga_item)
        if (products.length > 0) {
          this.$gtmTrack.ecomCart(products, data)
        }
      })
  },
  cartQtyChanged ({ state, commit }, { curProduct, type, quantity }) {
    commit('updateCartItemQty', { productId: curProduct.product_id, type, quantity })
    this.$api.myShop.setCart({ quantity, productId: curProduct.cart_id })
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  cartSaveAll ({ commit }) {
    this.$api.myShop.saveCart()
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  cartRestoreAll ({ commit }) {
    this.$api.myShop.restoreCart()
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  cartDeleteAll ({ commit }) {
    this.$api.myShop.deleteCart()
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  wishlistDeleteAll ({ commit }) {
    this.$api.myShop.deleteWishList()
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  async waitlistAdd ({ commit }, product_id) {
    logger('save', 'waitlist', { good: product_id, is_not_aval: true })
    await this.$api.myUtil.setWaitList({ action: 'add', id: product_id })
      .then(({ data }) => {
        if (data) {
          commit('updateWaitlistAdded')
          commit('addToWaitList', product_id)
        } else {
          const q = this.$router.currentRoute.query.add_to_waitlist ? '' : (Object.keys(this.$router.currentRoute.query).length > 0 ? '&' : '?') + 'add_to_waitlist=' + product_id
          this.$router.push({ name: 'login', params: { redirect: this.$router.currentRoute.fullPath + q } }).then()
        }
      })
  },
  cartDeleteClicked ({ commit }, { curProduct, quantity, event }) {
    if (event) logger('delete', event, { good: +curProduct.cart_id })
    this.$api.myShop.deleteProduct(curProduct.cart_id)
      .then(({ data }) => {
        commit('updateCart', data)
        this.$api.getGTMItems(curProduct.cart_id)
          .then(({ data }) => {
            this.$gtmTrack.ecomRemove(data.items, quantity)
          })
      })
  },
  cartDeleteSelected ({ commit }, { goods, event }) {
    if (event) logger('cart_delete_array', event, { goods })
    const goods_id = goods.join()
    this.$api.myShop.deleteProduct(goods_id)
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  wishlistDeleteClicked ({ commit }, { curProduct, event }) {
    if (event) logger('delete_save', event, { good: curProduct.cart_id })
    this.$api.myShop.deleteProduct(curProduct.cart_id)
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  cartBuyClicked ({ getters, commit }, { curProduct, quantity, extra, event, list }) {
    quantity = quantity || 1
    extra = extra || ''
    const { availability } = curProduct
    // проверка доступности товара
    if (availability && availability.count !== null) {
      const inCart = getters.qtyOfProductInCart(curProduct.product_id)
      const q = quantity + inCart
      if (extra !== '&save=1' && q > availability.count) {
        quantity = availability.count - inCart
        if (quantity > 0) {
          if (!confirm(`К сожалению, в наличии только ${quantity} шт. Добавить ${quantity} шт. в корзину?`)) {
            return
          }
        } else {
          alert(`К сожалению, в наличии данного товара только ${availability.count} шт.`)
          return
        }
      }
    }
    if (event) logger('submit', event, { good: curProduct.product_id })
    return this.$api.myShop.addCart({ quantity, productId: curProduct.product_id, extra })
      .then(({ data }) => {
        commit('updateCartPartial', data)
        document.getElementById('app').__vue__.$emit('cartitemadded', { product_id: curProduct.product_id, save: extra === '&save=1' })
        if (extra === '') {
          this.$gtmTrack.ecomAdd(curProduct.ga_item, list, quantity)
        }
      })
  },
  cartSaveClicked ({ state, commit, dispatch }, { curProduct, event }) {
    if (state.cart.cartObj[curProduct.product_id] > 0) {
      // перенести в избранное товар, который уже в корзине
      return dispatch('cartSave1Clicked', { curProduct, event })
    } else {
      // перенести в избранное товар, который не в корзине
      if (event) logger('save', event, { good: curProduct.cart_id })
      const p = dispatch('cartBuyClicked', { curProduct, quantity: 1, extra: '&save=1' })
      this.$gtmTrack.wishlistAdd([curProduct.ga_item], curProduct.cost)
      return p
    }
  },
  cartSave1Clicked ({ commit }, { curProduct, event }) {
    if (event) logger('save', event, { good: curProduct.cart_id })
    this.$api.myShop.saveCart(curProduct.cart_id)
      .then(({ data }) => {
        commit('updateCart', data)
        document.getElementById('app').__vue__.$emit('cartitemadded', { product_id: curProduct.cart_id, save: true })
        const products = data.cart.map(item => item.ga_item)
        if (products.length > 0) {
          this.$gtmTrack.wishlistAdd(products)
        }
      })
  },
  cartSaveSelected ({ commit }, { goods, event }) {
    if (event) logger('save_array', event, { goods })
    const goods_id = goods.join()
    this.$api.myShop.saveCart(goods_id)
      .then(({ data }) => {
        commit('updateCart', data)
      })
  },
  cartRestoreClicked ({ commit }, { curProduct, event }) {
    // вернуть в корзину товар из избранных
    this.$api.myShop.restoreCart(curProduct.cart_id)
      .then(({ data }) => {
        commit('updateCart', data)

        this.$gtmTrack.ecomAdd(curProduct.ga_item, 'Избранное')
      })

    if (event) logger('submit', event, { good: curProduct.cart_id })
  },
  orderClicked ({ commit }, { event }) {
    logger('goto', event, { goto: 'to_order' })
    this.$router.push({ name: 'new-order' })
  },
  async loadProduct ({ state, commit }, { id }) {
    commit('showLoader')
    commit('updateCurProduct', {})
    try {
      const { data } = await this.$api.myShop.getProduct(id)
      if (data) {
        if (data.redirect) {
          return this.$router.replace(data.redirect)
        }
        if (data.product) commit('updateCurProduct', data.product)
        if (data.meta) commit('updatePageMeta', data.meta)
        if (data.cart) commit('updateCartPartial', data.cart)
        this.$gtmTrack.googleTagParams(id, 'product', state.curProduct.cost)
        this.$gtmTrack.ecomDetail([data.product.ga_item])
      } else {
        commit('updateCurProduct', { availability: { status: 0 } })
      }
    } finally {
      commit('hideLoader')
    }
  },
  async loadRecommends ({ state, commit }, id) {
    const product_id = id || null
    await this.$api.myShop.getRecommends(product_id)
      .then(({ data }) => {
        commit('updateRecommends', data.recs)
        let product_ids
        const products_cost = {}
        data.recs.products.forEach(function (rec) {
          product_ids = product_ids + '_' + rec.product_id
          products_cost[rec.product_id] = rec.cost
        })
        if (product_ids) {
          this.$api.getGTMItems(product_ids)
            .then(({ data }) => {
              this.$gtmTrack.ecomList(data.items, 'С этим товаром покупают')
            })
        }
      })
  },
  getClientData ({ commit }) {
    return this.$api.myUtil.getClientData()
      .then(({ data }) => {
        commit('setClient', data.client)
        commit('updateCartPartial', data.cart)
        commit('updateWaitList', data.client.waitlist_product_ids || [])
        return data
      })
      .catch(e => console.error('getInitialData', e))
  },
  vuePageView ({ state, getters }, { name, fullPath }) {
    this.$gtmTrack.vuePageView(name, getters.productBreadcrumbs, state.client.id, document.location.href, fullPath, state.pageMeta.title)
  },
  sendNotifications ({ commit }, query) {
    this.$api.myUtil.setNotifications(query)
      .then(({ data }) => {
        if (data.status === 'success') {
          commit('updateNotifications', { [query.key]: query.value })
        }
        return data
      })
      .catch(e => console.error(e))
  },
  checkViewNotifications ({ getters, commit, dispatch }, key) {
    const viewCount = key in getters.notifications ? Number(getters.notifications[key]) : 0
    if (viewCount === 3) {
      dispatch('sendNotifications', { key, value: 4 })
    } else if (viewCount < 4) {
      commit('updateNotifications', { [key]: viewCount + 1 })
    }
  },
  setHelpHintModal ({ commit, dispatch }, props) {
    if (props && props.setMode) {
      commit('modals/setStatus', props.name ? props.name : null)
      commit('modals/setName', props ? props.modal : null)
    }
    dispatch('modals/toggle', { name: props ? props.modal : null })
    commit('setHelpHint', props)
  },
  getPartners ( ctx, { partner, product_id, pin }) {
    if (partner) {
      let query = `&id=${partner}`
      if (product_id) query += `&product_id=${product_id}`
      if (pin) query += `&pin=${pin}`
      this.$api.myShop.getPartners(query)
        .then(() => {})
    }
  },
  getViewId ({ state, commit }) {
    if (state.viewId) {
      process.client && (window.myshop = { ...window.myshop, view_id: state.viewId })
      return state.viewId
    }

    const d = new Date();
    const k = d.getTime();
    const str = k.toString(16).slice(1)
    const UUID = 'xxxxxxxx-xxxx-4xxx-yxxx-xzxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = Math.random() * 16 | 0
      const v = c === 'x' ? r : (r & 3 | 8)
      return v.toString(16)
    });

    const newId = UUID.replace(/[z]/, str)
    process.client && (window.myshop = { ...window.myshop, view_id: newId })
    commit('setViewId', newId)

    return  newId
  },
  async nuxtServerInit({ dispatch }) {
    await dispatch('application/getInitData')
  }
}
