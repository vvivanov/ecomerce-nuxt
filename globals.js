import { addZeros } from '@/helpers/util'

const yandex_maps_apikey = '6c73a11b-216f-4bf7-beb0-77c9e0b453de'

export const shipmentDateTitles = {
  0: 'ориентировочная дата отгрузки',
  1: 'планируемая дата доставки'
}

export const myDeliveryNames = {
  0: 'курьером',
  1: 'на почту',
  2: 'самовывоз',
  3: 'на почту',
  4: 'на почту',
  5: 'на почту',
  6: 'курьером',
  7: 'курьером',
  8: 'в пункт выдачи',
  9: 'курьером'
}

export const paymentCodes = {
  0: 'При получении',
  1: 'Банковский перевод',
  2: 'Банковская&nbsp;карта онлайн',
  201: 'Apple pay',
  202: 'Google pay',
  3: 'WM Transfer',
  4: 'Наложенный платёж',
  5: 'Почтовый перевод',
  6: 'Банковская&nbsp;карта онлайн',
  7: 'Счёт в My-shop.ru',
  9: 'QR-код онлайн'
}

export const days = [...Array.from({ length: 31 }, (_, i) => { return { value: addZeros(i + 1).toString(), text: (i + 1).toString() } })]

export const months = [
  { value: '01', text: 'Января' },
  { value: '02', text: 'Февраля' },
  { value: '03', text: 'Марта' },
  { value: '04', text: 'Апреля' },
  { value: '05', text: 'Мая' },
  { value: '06', text: 'Июня' },
  { value: '07', text: 'Июля' },
  { value: '08', text: 'Августа' },
  { value: '09', text: 'Сентября' },
  { value: '10', text: 'Октября' },
  { value: '11', text: 'Ноября' },
  { value: '12', text: 'Декабря' }
]

/* 0 — аннулирован
 * 1 — [old]
 * 2 — ожидание предоплаты
 * 3 — в обработке, см. местонахождение заказа (Комплектация: FFEAB3, Доставка: B3E1F6)
 * 4 — ожидание ответа
 * 5 — [old] отправлен по почте, но ещё не оплачен
 * 6 — доставлен и оплачен */
export const myOrderColors = ['#D4D8DA', '#FFC0FF', '#FFDAB3', '#FFEAB3', '#F5DEB3', '#5F9EA0', '#D0E5B7']

export function propFetch (base, names) {
  if (typeof names === 'string') {
    names = names.split('.')
  }
  const l = names.length
  let i = 0
  let tmp = base
  for (i = 0; i < l; ++i) {
    const name = names[i]
    if (typeof tmp !== 'object' && typeof tmp !== 'function') return
    base = tmp
    tmp = tmp[name]
  }
  return tmp
}

export function getViewId () {
  let t = null

  if (process.client) {
    t = propFetch(window, 'myshop.view_id')
    if (t) return t
    t = uuidv4()
    setViewId(t)
  }
  
  return t
}

export function setViewId (uuid) {
  const mh = window.myshop = window.myshop || {}
  mh.view_id = uuid
}

/**
 *
 * @description генерация уникального view_id страницы
 */
export function uuidv4 () {
  // From: https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid
  /* Original:
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
  */
  let getHex = function (c) {
    return (c ^ Math.random() * 16 >> c / 4).toString(16)
  }
  if (window.crypto && crypto.getRandomValues) {
    getHex = function (c) {
      return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    }
  }
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, getHex)
}

export const presenceFilters = [
  { href: '0', title: 'Все' },
  { href: '2', title: 'В наличии' },
  { href: 's', title: 'В акциях' },
  { href: '3', title: 'На складе' }
]

export const acDefaultOptions = [
  { value: 'г. Москва', ao_guid: '0c5b2444-70a0-4932-980c-b4dc0d3f02b5', region_id: 292 },
  { value: 'г. Санкт-Петербург', ao_guid: 'c2deb16a-0330-4f05-821f-1d09c93331e6' },
  { value: 'г. Новосибирск, Новосибирская обл.', ao_guid: '8dea00e3-9aab-4d8e-887c-ef2aaa546456' },
  { value: 'г. Екатеринбург, Свердловская обл.', ao_guid: '2763c110-cb8b-416a-9dac-ad28a55b4402' },
  { value: 'г. Нижний Новгород, Нижегородская обл.', ao_guid: '555e7d61-d9a7-4ba6-9770-6caa8198c483' },
  { value: 'г. Казань, Татарстан респ.', ao_guid: '93b3df57-4c89-44df-ac42-96f05e9cd3b9' },
  { value: 'г. Челябинск, Челябинская обл.', ao_guid: 'a376e68d-724a-4472-be7c-891bdb09ae32' },
  { value: 'г. Омск, Омская обл.', ao_guid: '140e31da-27bf-4519-9ea0-6185d681d44e' },
  { value: 'г. Самара, Самарская обл.', ao_guid: 'bb035cc3-1dc2-4627-9d25-a1bf2d4b936b' },
  { value: 'г. Ростов-на-Дону, Ростовская обл.', ao_guid: 'c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5' },
  { value: 'г. Уфа, Башкортостан респ.', ao_guid: '7339e834-2cb4-4734-a4c7-1fca2c66e562' },
  { value: 'г. Красноярск, Красноярский край', ao_guid: '9b968c73-f4d4-4012-8da8-3dacd4d4c1bd' }
]

// FIXME: переименован, потому что конфликтует
export const paymentCodesNewOrder = {
  0: 'При получении',
  1: 'Банковский перевод',
  2: 'Сейчас',
  201: 'Apple pay',
  202: 'Google pay',
  6: 'Сейчас',
  7: 'Счёт в My-shop.ru',
  9: 'QR-код онлайн'
}

export const defaultPVZFilters = {
  contractors: [],
  dlvDates: [],
  dlvTypes: [],
  payments: [],
  others: []
}

export const yandexGeocoder = (addr, precision = 'other') => {
  const url = `https://geocode-maps.yandex.ru/1.x/?format=json&apikey=${yandex_maps_apikey}&geocode=${encodeURIComponent(addr)}`

  return fetch(url)
    .then(response => response.json())
    .then(({ response: { GeoObjectCollection: { featureMember: features } } }) => {
      const result = {}

      if (features && features.length) {
        const { GeoObject: { metaDataProperty: { GeocoderMetaData: meta }, Point: { pos }, name } } = features[0]

        if (meta && meta.precision) {
          if (meta.precision !== precision && pos) {
            const [lon, lat] = pos.split(' ')

            result.lon = lon
            result.lat = lat
          }

          if (meta.precision === 'exact' && meta.kind && meta.kind === 'house') {
            result.name = name
          }
        }
      }

      return result
    })
}

export const getDadata = (body) => {
  const url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address'

  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: 'Token f373e937fb0bda15ab5ef5710515ba62825d79b7' // FIXME: токен с сервера
    },
    body: JSON.stringify(body)
  })
    .then(response => response.json())
    .catch(error => console.error('Dadata failed', error)) // eslint-disable-line no-console
}

export const loadMaps = () => {
  const url = `https://api-maps.yandex.ru/2.1/?apikey=${yandex_maps_apikey}&lang=ru_RU`

  const script = document.createElement('script')
  script.setAttribute('src', url)
  document.head.appendChild(script)
}

/**
 * @see https://s.my-shop.ru/cgi-bin/admin/mycms_db.pl поле 223_6
 */
export const ClaimType = Object.freeze({
  0: {
    name: 'Недостача'
  },
  1: {
    name: 'Излишки'
  },
  2: {
    name: 'Получен не тот товар'
  },
  3: {
    name: 'Брак (нетоварный вид)'
  },
  4: {
    name: 'Брак (скрытый)'
  },
  9: {
    name: 'Прочее'
  },
  100: {
    name: 'Возврат товара'
  }
})
