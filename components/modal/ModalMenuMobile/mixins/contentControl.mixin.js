const contentControl = {
  data () {
    return {
      content_prevItem: {},
      content_currentItem: {},
      content_nextItem: {},
      content_parentStack: []
    }
  },

  methods: {
    content_setNextItem (targetItem) {
      this.content_nextItem = targetItem
    },
    content_setPrevItem () {
      this.content_prevItem = this.content_parentStack[this.content_parentStack.length - 1]
    },
    content_homingItemAfterNext () {
      this.content_prevItem = this.content_currentItem
      this.content_currentItem = this.content_nextItem
      this.content_nextItem = {}
    },
    content_homingItemAfterBack () {
      this.content_parentStack.pop()
      this.content_currentItem = this.content_prevItem
      this.content_nextItem = {}
      if (this.content_prevItem.root) this.content_parentStack = []
    },
    content_pushCurrentToParentStack () {
      const parent = this.content_currentItem
      this.content_parentStack.push(parent)
    },
    content_clear () {
      this.content_prevItem = {}
      this.content_currentItem = this.content_parentStack.length > 0 ? this.content_parentStack[0] : this.content_currentItem
      this.content_nextItem = {}
      this.content_parentStack = []
    }
  }
}

export default contentControl
