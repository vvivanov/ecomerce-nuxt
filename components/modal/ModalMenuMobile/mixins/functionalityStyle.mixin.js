const functionalityStyle = {
  data () {
    return {
      style_wrapperStyle: {},
      style_wrapperActiveStyle: {},
      style_panelStyle: {},
      style_transitionStyle: {}
    }
  },
  mounted () {
    const menuOpenSpeed = this.menuOpenSpeed
    const menuSwitchSpeed = this.menuSwitchSpeed

    const menuOpenTransitionSecond = `.${menuOpenSpeed / 10}s`
    const menuSwitchTransitionSecond = `.${menuSwitchSpeed / 10}s`

    const wrapperStyle = {
      width: '100vw',
      position: 'absolute',
      top: '60px',
      left: '-100vw',
      height: '100%',
      zIndex: 99999,
      overflow: 'hidden',
      overflowY: 'scroll',
      transition: `left ${menuOpenTransitionSecond}`
    }

    const wrapperActiveStyle = {
      left: 0
    }

    const panelStyle = {
      position: 'absolute',
      top: 0,
      height: '100%',
      width: '100%',
      padding: '1rem',
      zIndex: 99999
    }

    const transitionStyle = {
      transition: `left ${menuSwitchTransitionSecond}`
    }

    this.style_wrapperStyle = wrapperStyle
    this.style_wrapperActiveStyle = wrapperActiveStyle
    this.style_panelStyle = panelStyle
    this.style_transitionStyle = transitionStyle
  }
}

export default functionalityStyle
