import Vue from 'vue'

const RenderSlide = Vue.component('RenderSlide', {
  props: {
    'components': {
      type: Object,
      require: true,
      default: () => {}
    }
  },
  render (h) {
    return h('div', {class: 'slider__slide-outer'}, [
      this.components
    ])
  }
})
export default RenderSlide