import SlideShow from './SlideShow'
import SlideShowSlide from './SlideShowSlide'


export {
    SlideShow,
    SlideShowSlide
}

/**
 * компоненты библиотеки слайдшоу слайдера
 * базовый пример
 * @example
 * <SlideShow
 *     :settings="settings"
 *     :window-width="windowWidth"
 *     :items="items.value"
 *     :height-slider="190"
 *   >
 *     <SlideShowSlide v-for="item in items.value" :key="item.id" ref="slide" >
 *       <img  :src="`https:${item.img}`"">
 *     </SlideShowSlide>
 *   </SlideShow>
 * @example
 *
 * @description формат объекта settings 
 *      {
 *        itemsToShow: 5.2, // кол-во слайдов которые будут показываться
 *        breakpoints: { // точки для изменения кол-ва слайдов - работают от меньшей в большую сторону
 *          1920: {
 *            itemsToShow: 7
 *          }
 *        }
 *      }
 *     
 */